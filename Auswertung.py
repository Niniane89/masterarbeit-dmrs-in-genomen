
import random
import pandas as pd
import Masterarbeit
import Simulation
import os
import time
from pprint import pprint
import cplex
import numpy as np
import pickle
import DataPlots


def simuliere_Rauschen(outfolder, cpgs = 10000):
	"""erzeugt einen kuenstlichen Datensatz, der keine DMR enthaelt"""
	#df mit der coverage
	coverage = pd.DataFrame([np.random.randint(20,30, 6) for _ in range(cpgs)], index = np.arange(0, 4*cpgs, 4), columns = np.arange(6))
	coverage.to_csv(outfolder+"/coverage.csv", sep= "\t")
	methyl = coverage.copy()
	
	classprob = 0.5 #startw'keit
	scale_class = 1/34.83
	scale_space = 1/33.97
	scale_diff = 1/16.25
	for pos, values in coverage.iterrows():
		mu_i_minus = truncate(np.random.laplace(classprob, scale_diff))
		mu_i_plus = truncate(np.random.laplace(classprob, scale_diff))
		methyl.loc[ pos, 0:2] = coverage.loc[ pos, 0:2].apply(lambda x: wuerfle_laplace(mu_i_minus,scale_class,x ))
		methyl.loc[ pos, 3:5] = coverage.loc[ pos, 3:5].apply(lambda x: wuerfle_laplace(mu_i_plus,scale_class,x ))
		classprob = truncate(np.random.laplace(classprob, scale_space))
	methyl.to_csv(outfolder+"/methyl"+str(0.5)+".csv", sep= "\t")


def truncate(x, lower = 0, top = 1):
	if x < 0:
		return 0
	if x> 1:
		return 1
	return x


def wuerfle_laplace(loc, scale, anzahl):
	"""wuerfelt fuer neue Reads deren Methylierungsstatus aus
	wkeit: Erfolgswahrscheinlichkeit pro Reads
	anzahl: Anzahl der neuen Reads"""
	prob = np.random.laplace(loc, scale)
	rand = random.random
	ergebnis = 0
	for _ in range(anzahl):
		if rand() < prob:
			ergebnis = ergebnis + 1
	#return ergebnis/anzahl
	return ergebnis


def simulieren_Daten(outfolder, cov_min =20, cov_max = 30, cpgs = 10000, dmr_len= 30):
	"""simuliert Testdaten. Fuer andere als die Standardparameter muessen die Startpositionen angepasst werden
	cov_min = Mindest-Coverage
	cov_max: Maximale Coverage
	cpgs : Anzahl CpGs in Testdaten
	dmr_len: Laenge der DMR"""
	#allgemeiner df mit der coverage

	coverage = pd.DataFrame([np.random.randint(cov_min,cov_max, 6) for _ in range(cpgs)], index = np.arange(0, 4*cpgs, 4), columns = np.arange(6))
	coverage.to_csv(outfolder+"/coverage.csv", sep= "\t")

	for md in np.arange(0.1, 1.01, 0.1):
		print(md)
		l = (1.0-md)/2
		u = l + md
		#unmethylierten df erstellen
		methyl = coverage.applymap(lambda x: wuerfle(0.5, x))
		#startpositionen der cpgs
		start_pos = [500 + j * 1000 for j in range(10) ] 
		for s in start_pos:

			methyl.iloc[ s: (s+dmr_len), 0:3] = coverage.iloc[ s: (s+dmr_len), 0:3].applymap(lambda x: wuerfle(l, x))
			methyl.iloc[ s: (s+dmr_len), 3:6] = coverage.iloc[ s: (s+dmr_len), 3:6].applymap(lambda x: wuerfle(u, x))
		methyl.to_csv(outfolder+"/methyl"+str(md)+".csv", sep= "\t")
		#print(methyl.iloc[500:550,:] / coverage.values[500:550,:])
		




def downsamplen(folder, coverage):
	"""erzeugt anhand der Methylierungsraten von echten Daten neue mit vorgegebener Coverage
	folder: Ordner der echten Daten
	coverage: Coverage der neuen Daten"""
	for filename in os.listdir(folder):
		frame = pd.read_csv(folder+filename, delim_whitespace = True, index_col = 0)
		frame.columns = ["M","U+M","M","U+M","M","U+M","M","U+M","M","U+M","M","U+M"]
		frame, _ = Masterarbeit.dezimal(frame)

		frame = frame.applymap(lambda x: wuerfle(x, coverage))

		frame.to_csv(folder +str(coverage)+"/"+ filename, sep= "\t", header = False)
	
		


def wuerfle(wkeit, anzahl):
	"""zieht anzahl reads mit W'keit wkeit"""
	rand = random.random
	ergebnis = 0
	for _ in range(anzahl):
		if rand() < wkeit:
			ergebnis = ergebnis + 1
	#return ergebnis/anzahl
	return ergebnis



def compare_synth_Data(folder, methylated_indices= set([4*((500+i) + j * 1000) for j in range(10) for i in range(30)]) ):
	"""wertet die Anzahl der falsch positiven und falsch negativen auf den Testdaten aus, gibt true positives, false positives
	und false negatives pro Schwellenwert aus.
	folder : der Ordner in dem sich die Daten befinden
	methylated_indices: die CpG-Positionen, an denen sich differenziell methylierte CpGs befinden"""

	result_A = list()
	result_B = list()
	result_C = list()
	
	#pprint(methylated_indices)
	for md in np.arange(0.1, 1.01, 0.1):
		print(md)
		all_indices = list(np.arange(0, 4*10000, 4)) #wenn die CpG-Positionen in simuliere_Daten() veraendert werden, dann auch hier
		#Ergebnisse der Optimierung einlesen
		frame = pd.read_csv(folder+"methyl"+str(md)+"Klassenmethylierung.csv", delim_whitespace = True, header = None, index_col = 0)
		result = frame.iloc[:,0]-frame.iloc[:,1]
		result = result[result.abs() > 10**(-6)]
		result = result.sort_index()
		result_df = pd.DataFrame(result)
		result = filter_DMR(result_df, all_indices, threshold = 10**(-6), min_CpGs = 4)
		if result is not None:
			result.to_csv(folder+str(md)+"results.bed", sep="\t")
			set1 = set(result.index)
		else:
			set1 = set()
		#uebereinstimmungen
		#richtig gefunden: index ist in methylated indices
		true_positives = set1.intersection(methylated_indices)
		false_positives = set1.difference(methylated_indices)
		
		false_negatives = methylated_indices.difference(set1)
		result_A.append(len(true_positives))
		result_C.append(len(false_positives))
		result_B.append(len(false_negatives))
		
	print(result_A)
	print(result_C)
	print(result_B)



def cpg_islands(folder):
	"""sucht in Daten nach CpG-Inseln (mindestens 100 CpGs deren Abstand nicht groesser als 20 ist"""
	filenames = []
	for filename in os.listdir(folder):
	#filenames.append(folder + filename )
	#filename = "chr8.bed"
		_,_, frame = Masterarbeit.read_Macaque_with_Pandas(folder+filename)
		name = filename.strip(".bed")
		akt_pos = 0
		for x, y in Masterarbeit.pairwise(frame.index):
			dist = y-x
			if dist <= 20:
				#neue CpG Insel starten
				if akt_pos == 0:
					akt_pos = x
			else:
				if akt_pos != 0:
					#CpG Insel beendet -> Ausgabe
					if frame.loc[akt_pos:y, :].shape[0] > 100:
						frame.loc[akt_pos:y, :].to_csv("cpgInseln/" + "CpG_Insel_"+name+"_"+str(akt_pos)+".csv", sep= "\t")
					akt_pos = 0
		del frame
		time.sleep(3)#ansonsten wird der speicher nicht frei gegeben


def compute_longest_piece(cut, df):
	"""berechnet die Groesse des laengsten zusammenhaengenden Stuecks, wenn nach einer gegebenen bp-Distanz aufgetrennt wird
	cut: Distanz, nach der aufgetrennt wird
	df: Dataframe, der Index muss die cpg -positionen enthalten"""

	longest = 0
	akt_pos = df.index[0]

	for x, y in Masterarbeit.pairwise(df.index):
		dist = y-x
		if dist >= cut:

			if( df.loc[akt_pos:x, :].shape[0] > longest):

				longest = df.loc[akt_pos:x, :].shape[0]
			akt_pos = y
		
	#am Ende angekommen
	if( df.loc[akt_pos:x, :].shape[0] > longest):
		longest = df.loc[akt_pos:y, :].shape[0]
	del df
	return longest


def compute_longest_pieces(folder):
	zuordnung = dict()
	
	distances = np.arange(100,210,10)
	#initialisieren
	for d in distances:
		zuordnung[d] = 0
	for filename in os.listdir(folder):
	
		case ,control , _ = Masterarbeit.read_Macaque_with_Pandas(folder+filename)
		frame = pd.concat([case, control], join='outer', axis = 1)
		for d in distances:

			a = compute_longest_piece(d, frame, filename.strip(".bed"))
			del frame
			if a > zuordnung[d]:
				zuordnung[d] = a
			#pprint(zuordnung)
		






def compare_dmr(result_folder, folder ="G:/Masterarbeit/Vergleich/filtered_0_1_3/"):
	"""vergleicht die Ergebnisse mit denen von BSmooth und Bisulfighter
	die Ergebnisse der anderen Verfahren muessen im uebergebenen Ordner und den Namen bisulfighter1_dmr.p, bisulfighter2_dmr.p
	und bsmooth_dmr.p zu finden sein"""

	result_ABC = list()
	result_A = list()
	#Schwellenwert variieren
	for t in np.arange(0.05,1, 0.05):
		A = set() #QP
		B = set() #BSmooth
		C = set() #Bisulfighter
	
		print(t)
		for i in range(1,21):
			chromosom = "chr"+str(i)
			outfolder = folder+chromosom+"/"
			#print(outfolder)
			bisul2_dmr = pickle.load(open( outfolder+"bisulfighter2_dmr.p","rb" ))
			bisul1_dmr = pickle.load(open( outfolder+"bisulfighter1_dmr.p","rb" ))
			bisul1_dmr.extend(bisul2_dmr)

			bsm_dmr = pickle.load(open( outfolder+"bsmooth_dmr.p","rb" ))
			if os.path.exists(result_folder+chromosom+"/"+str(t)+"_DMR.bed"):
				result = pd.read_csv(result_folder+chromosom+"/"+str(t)+"_DMR.bed", delim_whitespace = True,  index_col = 0)
				set1 = set((i,y) for y in result.index)
				A = A.union(set1)
			set2 = set((i,y) for y in bsm_dmr)
			set3 = set((i, y) for y in bisul1_dmr)


			B = B.union(set2)
			C = C.union(set3)
		ABC = A.intersection(B,C)
		AB = (A.intersection(B)).difference(C)
		AC = (A.intersection(C)).difference(B)
		BC = (B.intersection(C)).difference(A)

		result_ABC.append(len(ABC))
		result_A.append(len(A))

		DataPlots.plot_venn_diagramm([A,B,C], ["Lösung des QP", "BSmooth", "Bisulfighter"],t)
		#DataPlots.plot_venn_diagramm([B,C], ["BSmooth","Bisulfighter"])
		with open(folder+str(t)+"vergleich.bed","w") as f:
			pprint("alle", f)
			pprint(ABC, f)
			pprint("QP+BSmooth", f)
			pprint(AB, f)
			pprint("QP+Bisulfighter", f)
			pprint(AC, f)
			pprint("BSmooth+Bisulfighter", f)
			pprint(BC, f)
			pprint("Nur QP", f)
			pprint(A.difference(B,C), f)
	print(result_ABC)
	print(result_A)


	

def compare_weight_variation():
	"""vergleicht die ergebnisse fuer variierte Gewichte
	Pfade der jeweiligen Ergebnisse sind hardgecoded und muessen bei bedarf geaendert werden"""

	result_Conly = list()
	result_A = list()
	result_B = list()
	result_C = list()
	result_D = list()
	#for t in np.arange(0.05,1, 0.05):
	for t in [0.2]:
		A = set()
		B = set()
		C = set()
		D = set()
		print(t)
		i = 16
		chromosom = "chr"+str(i)
		outfolder = folder+chromosom+"/"
		if os.path.exists("G:/Masterarbeit/cplex/adiff_halbiert/"+chromosom+"/"+str(t)+"_DMR.bed"):
			result = pd.read_csv("G:/Masterarbeit/cplex/adiff_halbiert/"+chromosom+"/"+str(t)+"_DMR.bed", delim_whitespace = True,  index_col = 0)
			set1 = set((i,y) for y in result.index)
			A = A.union(set1)
		if os.path.exists("G:/Masterarbeit/cplex/3aspace/"+chromosom+"/"+str(t)+"_DMR.bed"):
			result = pd.read_csv("G:/Masterarbeit/cplex/3aspace/"+chromosom+"/"+str(t)+"_DMR.bed", delim_whitespace = True,  index_col = 0)
			set2 = set((i,y) for y in result.index)
			B = B.union(set2)

		if os.path.exists("G:/Masterarbeit/cplex/3aspace_adiff_halbiert/"+chromosom+"/"+str(t)+"_DMR.bed"):
			result = pd.read_csv("G:/Masterarbeit/cplex/3aspace_adiff_halbiert/"+chromosom+"/"+str(t)+"_DMR.bed", delim_whitespace = True,  index_col = 0)
			set3 = set((i,y) for y in result.index)
			C = C.union(set3)

		if os.path.exists("G:/Masterarbeit/cplex/normal/"+chromosom+"/"+str(t)+"_DMR.bed"):
			result = pd.read_csv("G:/Masterarbeit/cplex/normal/"+chromosom+"/"+str(t)+"_DMR.bed", delim_whitespace = True,  index_col = 0)
			set4 = set((i,y) for y in result.index)
			D = D.union(set4)

		print("nur adiff")
		pprint(A.difference(B,C,D))

		print("nur kombi")
		
		Conly = C.difference(A,B,D)
		pprint(Conly)

		print("nur schnitt")
		pprint((A.intersection(C)).difference(B,D))
	
		#print(len(AB))
		#if len(C) > 0:
			#DataPlots.plot_venn_diagramm([A,C], ["0.5 adiff", "kombiniert"], t)
			#DataPlots.plot_venn_diagramm([A,B,C], ["0.5 adiff", "3 aspace", "kombiniert"], t)
			#DataPlots.plot_venn_diagramm([A,C,D], ["0.5 adiff",  "kombiniert", "normal"], t)
		#if len(B) > 0:
			#DataPlots.plot_venn_diagramm([B,D], ["3 aspace", "normal"], t)
		result_Conly.append(len(Conly))
		result_A.append(len(A))
		result_B.append(len(B))
		result_C.append(len(C))
		result_D.append(len(D))
	print(result_A)
	print(result_B)
	print(result_C)
	print(result_D)
	print(result_Conly)









def prepare_comparison(outf):
	"""filtert die Ergebnisse von BSmooth und Bisulfighter nach den gegebenen Kriterien und schreibt diese in name_dmr.p
	outf: Zielordner fuer die Dateien"""
	threshold = 0.1
	bisul_thresh = 3
	for i in range(1,22):
		chromosom = "chr"+str(i)
		if i == 21:
			chromosom = "chrX"
		all_indices = pickle.load(open( "G:/Masterarbeit/indices/"+chromosom+"_indices.p", "rb"))

		bisulfighter1 = pd.read_csv("Ergebnisse_Bisulfighter_hypo.bed",delim_whitespace = True,  index_col = 0, usecols = [0,1,2,4]).loc[chromosom]
		bisulfighter2 = pd.read_csv("Ergebnisse_Bisulfighter_hyper.bed",delim_whitespace = True,  index_col = 0, usecols = [0,1,2,4]).loc[chromosom]
		bsmooth = pd.read_csv("Ergebnisse_BSmooth_neu.bed",delim_whitespace = True,  index_col = 0, usecols = [0,1,2,3]).loc[chromosom]
		

		bsmooth = bsmooth[bsmooth.iloc[:,2].abs() >= threshold]
		bisulfighter1 = bisulfighter1[bisulfighter1.iloc[:,2]>= bisul_thres]
		bisulfighter2 = bisulfighter2[bisulfighter2.iloc[:,2]>= bisul_thres]

		
		bisul1, bisul1_dmr = methylated_indices(list(all_indices), bisulfighter1, True)
		bisul2, bisul2_dmr = methylated_indices(list(all_indices), bisulfighter2, True)
		bsm, bsm_dmr = methylated_indices(all_indices, bsmooth)

		outfolder = outf+chromosom+"/"
		pickle.dump(bisul1, open( outfolder+"bisulfighter1_indices.p","wb" ))
		pickle.dump(bisul2, open( outfolder+"bisulfighter2_indices.p","wb" ))
		pickle.dump(bsm, open( outfolder+"bsmooth_indices.p","wb" ))

		pickle.dump(bisul1_dmr, open( outfolder+"bisulfighter1_dmr.p","wb" ))
		pickle.dump(bisul2_dmr, open( outfolder+"bisulfighter2_dmr.p","wb" ))
		pickle.dump(bsm_dmr, open( outfolder+"bsmooth_dmr.p","wb" ))






def methylated_indices(all_indices, df, bisul = False, dmr_thres = 4):
	"""schreibt aus den Bsmooth und bisulfighter ergebnissen die indices raus
	all_indices: Liste mit den Positionen von ALLEN CpGs
	df: zu filternde Ergebnisse, in jeder Zeile sind start und end der dmr gegeben
	bisul: Flag ob die ergebnisse zu bsmooth oder bisulfighter gehoeren, die unterscheiden sich jeweils um eine position
	dmr_thres: mindestgroesse der dmr

	Ausgabe:
	result: Liste aller diff.-methylierten Positionen
	dmr: Liste aller CpG-Positionen in DMR"""
	result = []
	dmr = []
	print(bisul)
	for _, row in df.iterrows():
		try:
			if bisul == True:
				end = all_indices.index(row["end"]-1) 
			else:
				end = all_indices.index(row["end"])
			cur_dmr = all_indices[all_indices.index(row["start"]) : end ]
			result.extend(cur_dmr)
			if len(cur_dmr) >= dmr_thres:
				dmr.extend(cur_dmr)
			#nicht immer von vorne suchen
			if end > 200:
				del all_indices[0:end]
		except ValueError:
			print(row["end"])
			print("not in list")
		

	return result, dmr



def write_indices():
	"""schreibt eine Liste aller CpG-Positionen raus"""
	for i in range(1,22):
		chromosom = "chr"+str(i)
		ind = list(pd.read_csv("G:/Masterarbeit/Makaken/"+chromosom+".bed",delim_whitespace = True,  index_col = 0, usecols = [0]).index)
		pickle.dump(ind , open( "G:/Masterarbeit/indices/"+chromosom+"_indices.p", "wb"))




def find_DMC(folder):
	"""alle cplex ergebnisse fuer ein Chromosom werden gemeinsam in eine Datei geschrieben"""

	for subfolder in os.listdir(folder):
		print(subfolder)
		chromosom_result = pd.Series()

		for filename in os.listdir(folder+subfolder+"/"):
			if "Klassenmethylierung" in filename:
				frame = pd.read_csv(folder+subfolder+"/"+filename, delim_whitespace = True, header = None, index_col = 0)
				result = frame.iloc[:,0]-frame.iloc[:,1]
				chromosom_result = chromosom_result.append(result)

		chromosom_result= chromosom_result.sort_index()
		chromosom_result.to_csv(folder+subfolder+"/"+"results.bed", sep="\t")


def dmcs_zusammenfassen(folder):
	"""schreibt die CPLEX-Ergebnisse von allen Chromosomen in eine Datei.
	Ein Aufruf von find_DMC muss voraus gehen."""
	overall_result = pd.Series()
	for subfolder in os.listdir(folder):
		print(subfolder)
		result = pd.read_csv(folder+subfolder+"/"+"results.bed", delim_whitespace = True, header = None, index_col = 0)

		tuples = [(subfolder, x) for x in result.index]
		index = pd.MultiIndex.from_tuples(tuples, names=['chr', 'pos'])
		result.index = index
		del tuples
		del index
		result = result[result.iloc[:,0].abs() > 10**(-6)	]
		#chromosom_index = result.reindex(index = index)
		overall_result = overall_result.append(result)
		del result
	overall_result.to_csv(folder+"QPresults.bed", sep="\t")
		






def filter_DMR(result, all_indices, threshold = 0.2, min_CpGs = 4):
	"""Filtert aus der Datei, die alle Methylierungsunterschiede enthaelt diejenigen raus, die groesser als epsilon sind
	result: DF mit den Ergebnissen, format: index methylierungsdifferenz
	all_indices: liste mit allen indices des chromosom"""
	dmrs = pd.DataFrame()
	result = result.abs()

	result = result[result.iloc[:,0] > threshold]
	print(result.shape)
	if result.empty:
		print("Keine DMCs gefunden")
		return
	akt_df = pd.DataFrame()
	#pprint(result.index)
	for i, j in Masterarbeit.pairwise(result.index):
		i_ind = all_indices.index(i)

		if all_indices.index(j) - i_ind == 1:
			#beide nebeneinander
			if akt_df.empty:
				akt_df = akt_df.append(result.loc[i])
			akt_df = akt_df.append(result.loc[j])
		else:
			#pruefe ob mehr als min_CpGs cpg
			if akt_df.shape[0] >= min_CpGs:
				dmrs = dmrs.append(akt_df)
			akt_df = pd.DataFrame()
		if i_ind > 200:
				del all_indices[0:i_ind]
	#dmr am ende
	if akt_df.shape[0] >= min_CpGs:
		dmrs = dmrs.append(akt_df)
	#print(dmrs)
	if dmrs.empty:
		return

	return dmrs










if __name__ == "__main__":
	compare_synth_Data()