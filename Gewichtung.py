
from operator import itemgetter
import numpy as np
import pandas as pd
import math
import os
import matplotlib.pyplot as plt
import scipy.optimize as op
import statsmodels.api as sm
from scipy.stats import expon
from scipy.stats import stats
import scipy
from collections import defaultdict
from pprint import pprint
import pickle
import DataPlots
from itertools import tee


def read_Macaque_with_Pandas(filename):
	""" Lies .bed Datei im Format CpG-Startposition Methylierungswert ein
	Ausgabe: DataFrame im Format CpG-Startposition Methylierungswerte
	filenames: Liste von Tupeln aus directory und filename"""
	frame = pd.DataFrame()
	dfs = []
	print(filename)
	frame = pd.read_csv(filename, delim_whitespace = True, index_col = 0)
	frame.columns = ["M","U+M","M","U+M","M","U+M","M","U+M","M","U+M","M","U+M"]
	case = frame.iloc[:,[0,1,6,7,10,11]]
	control= frame.iloc[:,[2,3,4,5,8,9]]
	pprint(frame.shape)
	return case, control, frame
	#return frame








	

def plot_lambdas_a_space(median, mean, id):
	ln_2 = math.log(2)
	lambda_mean = [(1 / m) for m in mean]
	lambda_median = [(ln_2 / m) for m in median]
	X = np.arange(2,26)


	DataPlots.create_plot("lambda", "Abstand in Basenpaaren", "Lambda")

	ax = plt.subplot(111)
	ax.plot(X, lambda_median, "b.", label = "Lambda für den gewichteten Median")
	ax.plot(X, lambda_mean, "r.", label = "Lambda für den gewichteten Mittelwert")
	DataPlots.set_legend_position_below(ax, 2)
	plt.savefig(id+"lambdas_a_space.pdf") #Speichert Bild in Datei
	plt.close()
	DataPlots.create_plot("lambda", "Abstand in Basenpaaren", "Methylierungsdifferenz")
	ax = plt.subplot(111)
	line = linear_regression_line(X, median)
	ax.plot(X, line, "b" )
	ax.plot(X, median, "b+" , label="Median")
	line_mean = linear_regression_line(X, mean)
	ax.plot(X, line_mean, "r" )
	ax.plot(X, mean, "r+" , label="Mittelwert")
	DataPlots.set_legend_position(ax)
	plt.savefig(id+"mean_median_aspace.pdf")
	plt.close()
	





def plot_weighted_median(pos_dist, Y, med, mean, id):
	"""plottet Median und Mittelwert , sowie die Regressionsgerade dadurch
	pos_dist und Y sind zusaetzlich die Datenpunkte"""

	DataPlots.create_plot("Methylierungsdifferenz_Median_Mean", "Abstand in Basenpaaren", "Methylierungsdifferenz")

	X = np.arange(2, max(pos_dist)+1)

	ax = plt.subplot(111)
	ax.plot(pos_dist, Y, "#84B818", marker = ".", linestyle = "None", label = "Datenpunkte")
	line = linear_regression_line(X, med)
	ax.plot(X, med,  "b+", label="Median")
	ax.plot(X, line, "b" )
	line_mean = linear_regression_line(X, mean)
	ax.plot(X, mean,  "r+", label="Mittelwert" )
	ax.plot(X, line_mean, "r" )

	DataPlots.set_legend_position(ax)
	plt.savefig(id+"a_space_Methylierungsdifferenz+median+mean.pdf")
	plt.close()


	


def weighted_median(X,Y, threshold, kernel = [10,9,8,7,6,5] ):
	"""berechnet den gewichteten Median. X CpG Abstaende, Y Methylierungswerte"""
	#print(np.isnan(Y).any())
	#print(np.isinf(Y).any())
	l= list(zip(X,Y))
	#Elemente in dictionary speichern, zu jedem x eine Liste der y Werte
	d = defaultdict( list )
	for k, v in l:
		d[k].append(v)

	w_median = []
	w_mean = []
	#alle cpG abstaende
	for pos in range(2,threshold):
		values = []
		#Die Nachbarpositionen
		for i in range(- len(kernel)+1,len(kernel)):
			if (pos+i) in d:
				v = d.get(pos+i)
				for _ in range(kernel[abs(i)]):
					values.extend(v)
				
		w_median.append(np.median(values))
		w_mean.append(np.mean(values, dtype=np.float64))
	return w_median, w_mean






def linear_regression_line(X,Y, filename=""):
	"""berechnet zu den x und y werten die lineare regressionslinie und gibt diese aus"""
	slope, intercept, r_value, p_value, std_err = stats.linregress(X,Y)
	y = [(slope* x +intercept) for x in X]

	print("slope: ", slope)
	print("intercept: ",intercept)
	print(r_value)
	print(p_value)
	print(std_err)
	return y








def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)



def compute_Meth_dist(frame, threshold_distance, threshold_coverage):
	'''berechnet den Abstand in Basenpaaren und die Methylierungsdifferenz benachbarter CpGs 
	Nur Positionen mit Basenpaarabstaenden <= threshold_distance und Coverage >= threshold_coverage werden beachtet
	Gibt die Positions- und Methylierungsdifferenzen als Liste zurueck'''
	X=[]
	Y=[]

	coverage = frame.loc[:,"U+M"]

	M = frame.loc[:,"M"]

	#berechne mu_i_t
	data = M/coverage.values[:,:] #dezimal, fuer jede Probe eine Spalte


	#jede Spalte einzeln betrachten
	for column in range(data.shape[1]):
		#Zeilenweise durchgehen
		for i, j in pairwise(frame.index):
			# Abstand in bp
			x = j-i
			if x <= threshold_distance:
				#Coverage muss auch groesser threshold sein
				if (coverage.loc[i].iloc[column] > threshold_coverage) and (coverage.loc[j].iloc[column] > threshold_coverage):

					y = abs(data.loc[i].iloc[column]-data.loc[j].iloc[column])

					if np.isnan(y) == False:
						if (np.isinf(y)) == False:
							X.append(x)
							Y.append(y)
	#hier X und Y speichern, um auf allen Chromosomen zu arbeiten
	del data
	del M
	del coverage
	return X, Y













def distance_diagonal(data_x, data_y):
	"""data_x und data_y muessen die gleiche Dimension haben,
	berechnet den Abstand von jedem Punkt aus (data_x, data_y) zur Diagonalen)"""
	delta = []
	for p_x, p_y in zip( data_x, data_y):
		m = (p_x+p_y)/2
		d = math.sqrt( math.pow(p_x-m,2)+ math.pow(p_y-m,2))
		delta.append(d)
	delta = sorted(delta, reverse = True)
	print(sum(delta))
	return delta	





	

def compute_distances_for_alpha_space(folder, threshold_distance = 1000, threshold_coverage = 100):
	"""berechnet die Distanzen fuer alpha space: Alle Abstaende zwischen cpgs, die nicht weiter als 
	threshold_distance bp-Abstand haben und eine coverage von mindestens
	threshold_coverage"""


	list_of_position_differences = []
	list_of_methylation_differences = []

	filenames = []
	for filename in os.listdir(folder):
		#filenames.append(folder + filename )
		_, _ , frame = read_Macaque_with_Pandas(folder+filename)
		name = filename.strip(".bed")
		pos_dist, meth_dist = compute_Meth_dist(frame, threshold_distance, threshold_coverage)
		del frame
		#list_of_position_differences.extend(pos_dist)
		#list_of_methylation_differences.extend(meth_dist)
	
		pickle.dump(pos_dist, open( "distances/list_of_position_differences"+name+".p", "wb" ) )
		pickle.dump(meth_dist, open( "distances/list_of_methylation_differences"+name+".p", "wb" ) )
		del pos_dist
		del meth_dist



def put_together_aspace(folder):
	"""setzt die in compute_distances_for_alpha_space errechneten Distanzen zusammen"""

	list_of_position_differences =[]
	list_of_methylation_differences =[]
	for filename in os.listdir(folder):
		name = filename.strip(".bed")
		print(name)
		try:
			list_of_position_differences.extend(pickle.load(open( "distances/list_of_position_differences"+name+".p", "rb" ) ))
			list_of_methylation_differences.extend(pickle.load(open( "distances/list_of_methylation_differences"+name+".p", "rb" ) ))
		except EOFError:
			print("error" + name)

	pickle.dump(list_of_position_differences, open( "distances/list_of_position_differences.p", "wb" ) )
	pickle.dump(list_of_methylation_differences, open( "distances/list_of_methylation_differences.p", "wb" ) )



def compute_alpha_space(folder):
	"""erfordert vorherigen aufruf von 1. compute_distances_for_alpha_space 2. put_together_aspace"""
	#alle Daten einlesen und konkatenieren
	
	X = pickle.load( open( "distances/list_of_position_differences.p", "rb" ) )
	Y = pickle.load( open("distances/list_of_methylation_differences.p", "rb" ) )


	DataPlots.create_plot("Methylierungsdifferenz", "Abstand in Basenpaaren", "Methylierungsdifferenz")
	ax = plt.subplot(111)
	ax.plot(X, Y, "#84B818", marker = ".", linestyle = "None")
	
	plt.savefig("a_space_Methylierungsdifferenz.pdf")

	#verschiedene (gewichtete) Mediane
	print("[10,9,8,7,6,5]")
	kernel = [10,9,8,7,6,5]
	med, mean = weighted_median(X,Y, 26, kernel)
	plot_weighted_median(X, Y, med, mean, "[10,9,8,7,6,5]")
	plot_lambdas_a_space(med, mean, "[10,9,8,7,6,5]")

	print("[9,8,7,6,5]")
	kernel = [9,8,7,6,5]
	med, mean = weighted_median(X,Y, 26, kernel)
	plot_weighted_median(X, Y, med, mean, "[9,8,7,6,5]")
	plot_lambdas_a_space(med, mean, "[9,8,7,6,5]")


	print("[1]")
	kernel = [1]
	med, mean = weighted_median(X,Y, 26, kernel)
	plot_weighted_median(X, Y, med, mean, "[1]")
	plot_lambdas_a_space(med, mean, "[1]")





def test_distribution(delta, alpha,  clas):
	"""testet die errechnete Exponentialverteilung mittels eines q-q-plots 
	delta: Die Messwerte
	alpha: Parameter der Exponentialfunktion
	clas: ein String zur Identifikation"""
	#delta sollte sortiert sein
	delta = sorted(delta)
	

	#
	#quantile
	n = len(delta)
	#P = [((i+1)/(n+1)) for i in range(n)]
	P = [((i+1)/(n+1)) for i in range(n)] #i 1..n  
	Quantile = [-(1/alpha)*math.log(1-p) for p in P]
	#axis = max(max(delta), max(Quantile))
	sc = 1/alpha
	#fig = sm.qqplot(np.array(delta), scipy.stats.expon, loc = 0, scale = sc, line = '45')
	#plt.show()
	distance_diagonal(delta, Quantile)
	filename = "QQPlot_"+str(alpha)+clas+".pdf"
	#plt.savefig(filename) #Speichert Bild in Datei
	plt.figure("name")
	plt.plot([0,0.25],[0,0.25], color = "k", marker = ".")
	plt.plot(delta, Quantile, color = "#84B818", marker = ".", linestyle="None")
	#plt.axis([0, 0.25, 0, 0.25]) #Achsen-Skalierung
	plt.xlabel("Delta") #Bezeichung x-Achse
	plt.ylabel("Quantile") #Bezeichnung y-Achse
	plt.savefig(filename) #Speichert Bild in Datei
	plt.close()



def sort(data):
	"""gibt fuer jede CpG Position die niedrigste Coverage aus"""
	n_i = data.loc[:,"U+M"]
	n_i_min = n_i.min(axis = 1)
	n_i_min.sort(ascending = False)
	return n_i_min


def dataFrame_as_list(frame):
	"""gibt die im Dataframe gespeicherten Werte als Liste zurueck"""
	result = []
	if len(frame.shape) == 1:
		result.extend(frame.values.tolist())

	else:
		for column in range(frame.shape[1]):

			result.extend(frame.iloc[:,column].values.tolist())
	return result


####################################################################################
#Berechnung von aclass und adiff
####################################################################################

def distance_subroutine(df):
	"""berechnet zu einem uebergebenen Dataframe (nur case ODER control) die Abstaende von muit zum Klassenmethylierungswert
	und den Klassenmethylierungswerten
	a: Dataframe mit den Abstaenden der Messwerte (als muit angenommen) zum Klassenmethylierungswert, der aus ihrem Mittelwert berechnet wird
	b: Dataframe mit den Abstaenden der Messwerte (als muit angenommen) zum Klassenmethylierungswert, der aus ihrem Median berechnet wird
	mu_class_median: Klassenmethylierungswert bestimmt aus dem Median
	mu_class_mean: Klassenmethylierungswert bestimmt aus dem Mittelwert"""
	M = df.loc[:,"M"]
	M_plus_U = df.loc[:,"U+M"]
	#berechne mu_i_t
	mu_i_t = M/M_plus_U.values[:,:] #dezimal, fuer jede Probe eine Spalte
	#berechne Klassenmethylierungswert auf 2 verschiedene Weisen
	mu_class_median = mu_i_t.median(axis = 1)
	mu_class_mean = mu_i_t.mean(axis = 1)
	
	#Abstaende von den mu_i_t zu den Klassenmethylierungswerten
	a = mu_i_t.sub(mu_class_mean, axis = 0).abs()
	b = mu_i_t.sub(mu_class_median, axis = 0).abs()
	del mu_i_t
	return a, b, mu_class_median, mu_class_mean






def compute_distances_for_alpha_class_and_diff(folder):
	'''nimmt die Methylierungswerte als mu_i_t an und berechnet jeweils den Abstand von diesen zum Mittelwert bzw Median
	werden in list_of_distances_mean.p, bzw list_of_distances_median.p gespeichert
	Zusaetzlich werden die Abstaende sortiert und geplottet'''
	filenames = []
	

	for filename in os.listdir(folder):
		#Listen mit den Abständen von mu_i_t zu jeweils median und mittelwert
		list_of_distances_mean = []
		list_of_distances_median = []

		list_of_class_distances_mean = []
		list_of_class_distances_median = []
		case, control, frame = read_Macaque_with_Pandas(folder+filename)
		#nur die Zeilen, die eine coverage von mindestens 200 aufweisen 
		sorted_frame = sort(frame)
		del frame
		frame_threshold = sorted_frame[sorted_frame>200]

		del sorted_frame
		case_threshold = case.loc[frame_threshold.index]
		control_threshold = control.loc[frame_threshold.index]
		del case
		del control

	
		if not case_threshold.empty and not control_threshold.empty:
			a, b, mu_class_median_case, mu_class_mean_case = distance_subroutine(case_threshold)
			list_of_distances_mean.extend(dataFrame_as_list(a))
			list_of_distances_median.extend(dataFrame_as_list(b))
			
			a, b, mu_class_median_control, mu_class_mean_control = distance_subroutine(control_threshold)
			list_of_distances_mean.extend(dataFrame_as_list(a))
			list_of_distances_median.extend(dataFrame_as_list(b))

			del control_threshold
			del case_threshold
			del frame_threshold
		
			list_of_class_distances_median.extend(dataFrame_as_list(mu_class_median_case.sub(mu_class_median_control, axis = 0).abs()))
			list_of_class_distances_mean.extend(dataFrame_as_list(mu_class_mean_case.sub(mu_class_mean_control, axis = 0).abs()))


			pickle.dump(list_of_distances_mean, open( "distances/list_of_distances_mean_"+filename.strip(".bed")+".p", "wb" ) )
			pickle.dump(list_of_distances_median, open( "distances/list_of_distances_median_"+filename.strip(".bed")+".p", "wb" ) )

			pickle.dump(list_of_class_distances_mean, open( "distances/list_of_class_distances_mean_"+filename.strip(".bed")+".p", "wb" ) )
			pickle.dump(list_of_class_distances_median, open( "distances/list_of_class_distances_median_"+filename.strip(".bed")+".p", "wb" ) )



def put_together(folder):
	"""fuegt nach aufruf von compute_distances_for_alpha_class_and_diff() die Ergebnisse aus allen Chromosomen zusammen"""
	list_of_distances_mean = []
	list_of_distances_median = []

	list_of_class_distances_mean = []
	list_of_class_distances_median = []

	for filename in os.listdir(folder):

		list_of_distances_mean.extend(pickle.load(open( "distances/list_of_distances_mean_"+filename.strip(".bed")+".p", "rb" ) ))
		list_of_distances_median.extend(pickle.load(open( "distances/list_of_distances_median_"+filename.strip(".bed")+".p", "rb" ) ))

		list_of_class_distances_mean.extend(pickle.load(open( "distances/list_of_class_distances_mean_"+filename.strip(".bed")+".p", "rb" ) ))
		list_of_class_distances_median.extend(pickle.load( open( "distances/list_of_class_distances_median_"+filename.strip(".bed")+".p", "rb" ) ))
	

	list_of_distances_mean = sorted(list_of_distances_mean)
	list_of_distances_median = sorted(list_of_distances_median)

	list_of_class_distances_mean = sorted(list_of_class_distances_mean)
	list_of_class_distances_median = sorted(list_of_class_distances_median)

	pickle.dump( list_of_distances_mean, open( "distances/list_of_distances_mean_all.p", "wb" ) )
	pickle.dump( list_of_distances_median, open( "distances/list_of_distances_median_all.p", "wb" ) )

	pickle.dump( list_of_class_distances_mean, open( "distances/list_of_class_distances_mean_all.p", "wb" ) )
	pickle.dump( list_of_class_distances_median, open( "distances/list_of_class_distances_median_all.p", "wb" ) )




def compute_alpha_diff(folder):
	'''berechnet verschiedene alpha diff und testet diese mit einem QQPlot'''
	#compute_distances_for_alpha_class(folder)
	
	list_of_distances_mean = pickle.load( open( folder+"list_of_class_distances_mean_all.p", "rb" ) )
	list_of_distances_median = pickle.load( open( folder+"list_of_class_distances_median_all.p", "rb" ) )



	list_of_distances_median= sorted(list_of_distances_median)
	n = len(list_of_distances_median)
	b = int(n/3)
	print(b)


	#list_of_distances_median = list_of_distances_median[b:n]
	#list_of_distances_median =   [i for i in list_of_distances_median if i > 0]



	ln_2 = math.log(2)
	alpha_1 = ln_2 / np.median(list_of_distances_mean)
	print(alpha_1)

	alpha_2 = 1 / np.mean(list_of_distances_mean)
	print(alpha_2)

	alpha_3 = ln_2 / np.median(list_of_distances_median)
	print(alpha_3)

	alpha_4 = 1 / np.mean(list_of_distances_median)
	print(alpha_4)

	#Alpha ueberpruefen
	test_distribution(list_of_distances_mean, alpha_1, 31.1, "adiff_Median_Abstaende_Mittelwert")
	test_distribution(list_of_distances_mean, alpha_2, 31.7, "adiff_Mittelwert_Abstaende_Mittelwert")
	test_distribution(list_of_distances_median, alpha_3, 30.4, "adiff_Median_Abstaende_Median")
	test_distribution(list_of_distances_median, alpha_4, 32.4, "adiff_Mittelwert_Abstaende_Median")


def compute_alpha_class(folder):
	#compute_distances_for_alpha_class(folder)
	
	list_of_distances_mean = pickle.load( open(folder+ "list_of_distances_mean_all.p", "rb" ) )
	list_of_distances_median = pickle.load( open( folder+"list_of_distances_median_all.p", "rb" ) )
	list_of_distances_median= sorted(list_of_distances_median)
	n = len(list_of_distances_median)
	b = int(n/3)
	print(b)
	list_of_distances_median = list_of_distances_median[b:n]
	#list_of_distances_median =   [i for i in list_of_distances_median if i > 0]

	ln_2 = math.log(2)
	alpha_1 = ln_2 / np.median(list_of_distances_mean)
	print(alpha_1)

	alpha_2 = 1 / np.mean(list_of_distances_mean)
	print(alpha_2)

	alpha_3 = ln_2 / np.median(list_of_distances_median)
	print(alpha_3)

	alpha_4 = 1 / np.mean(list_of_distances_median)
	print(alpha_4)

	#Alpha ueberpruefen
	test_distribution(list_of_distances_mean, alpha_1, 42.99, "Median_Abstaende_Mittelwert")
	test_distribution(list_of_distances_mean, alpha_2, 42.76, "Mittelwert_Abstaende_Mittelwert")
	test_distribution(list_of_distances_median, alpha_3, 34.38, "Median_Abstaende_Median")
	test_distribution(list_of_distances_median, alpha_4, 32.83, "Mittelwert_Abstaende_Median")



	

if __name__ == "__main__":

	#aclass und diff bestimmen:
	folder = "G:/Masterarbeit/Makaken/"
	compute_distances_for_alpha_class_and_diff(folder)
	#put_together(folder)
	#compute_alpha_class(folder)
	#compute_alpha_diff(folder)

	
	compute_distances_for_alpha_space(folder, 25, 200)
	#put_together_aspace(folder)
	#compute_alpha_space(folder)

	

