Masterarbeit  - Ein optimierungsbasierter Ansatz zur Identifikation differenziell methylierter Regionen in Genomen

Anforderung:

* Python 3.4.1

* pandas 0.14.1

* matplotlib 1.4.2

* numpy 1.9.0

* cplex 1.5.2

Gewichtung.py: Bestimmung der Gewichte der Zielfunktion

Masterarbeit.py: iterativer Ansatz inklusive der Erweiterungen

cplex.py: Anbindung an cplex

Auswertung.py: Erzeugung von Testdaten und Auswertung

graphics.py: Vergleichsplots