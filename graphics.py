# amplilyzer.graphics
# (c) Sven Rahmann, 2011--2013
#modified by Nina Hesse, 2014--2015

"""
This module provides plotting routines for amplikyzer.
It does not implement a subcommand.
"""

import sys

#########################################################
# safe import of plotting library

_FORMAT = None  # global memory of format during initial import

def import_pyplot_with_format(format):    
    """
    import matplotlib with a format-specific backend;
    globally set 'mpl' and 'plt' module variables.
    """
    if _FORMAT is None:
        _import_matplotlib(format)  # globally sets plt = matplotplib.pyplot
    if format != _FORMAT:
        raise RuntimeError(
            "Cannot use different formats ({}/{}) in the same run.\nPlease restart amplikyzer.".format(_FORMAT, format))

def _import_matplotlib(format):
    global np, mpl, plt
    BACKENDS = dict(png='Agg', pdf='Agg', svg='svg')
    # using 'pdf' (instead of 'Agg') for pdf results in strange %-symbols
    import numpy as np
    import matplotlib as mpl
    mpl.use(BACKENDS[format])
    import matplotlib.pyplot as plt
    global _FORMAT
    _FORMAT = format



def set_alpha(N, i, j):
    val = N[i, j]
    if val > 20:
        val = 20
    alpha = val /20
    return alpha

def mycmap(x, N):
    #customized colormap. Alphachannel depends on Coverage
    colors = ["#4444dd", "#dd4444"]  # (blue -> red)
    fontcolor = lambda x: "#ffffff"
    mycolormap = mpl.colors.LinearSegmentedColormap.from_list("mycolormap", colors)

    tmp = mycolormap(x)
    #print(tmp)
    for i in range(tmp.shape[0]):
        for j in range(tmp.shape[1]):
            tmp[i,j][3] = set_alpha(N, i, j)
            #tmp[i,j][3] = 1.0
    return tmp

#########################################################
# comparative methylation plot

def plot_comparative(C, N, fname, format="pdf", style="color", options=None, show_results = False, results = None):
    """
    Create and save a comparative methylation plot.
    C: a matrix of methylation counts
    N: a matrix of methylated + unmethylated counts
    analysis: an instance of methylation.ComparativeAnalysis
    fname: filename of the resulting image file
    format: image format (e.g., 'png', 'pdf', 'svg')
    style: image style ('color' or 'bw')
    Return True if successful, False when CpGs are inconsistent.
    """
    if options is None:  options = dict()

    analysis = C / N.values[:,:]


    # determine cpg positions or ranks
    cpgpos = analysis.columns
    if cpgpos is None:  return False  # inconsistent CpGs
    posstr = "positions"
    
    m, n = analysis.shape
    assert n is not None
    import_pyplot_with_format(format)

    # determine colormap        
    if style ==  "color":
        colors = ["#4444dd", "#dd4444"]  # (blue -> red)
        fontcolor = lambda x: "#ffffff"
    else:
        colors = ["#ffffff", "#000000"]  # (white -> black)
        fontcolor = lambda x: "#ffffff" if x>0.5 else "#000000"
    mycolormap = mpl.colors.LinearSegmentedColormap.from_list("mycolormap", colors)


    # initialize figure, set figure title/remark and axis title (subtitle)
    fig = plt.figure()
    titles = ["Vergleich: " + fname]
    yheight = 0.8
    #if analysis.remark is not None:
        #titles.append(analysis.remark)
        #yheight = 0.77
    subtitles = ["{} Proben, {} CpGs".format(m, n)]
    title = "\n".join(titles)
    subtitle = "\n".join(subtitles)
    fig.suptitle(title, fontsize=14, x=0.54)
    # if there is not enough space for labels at the left side,
    # increase the 'left' coordinate and reduce the 'width' in the following line
    ax = fig.add_axes([0.12, 0.1, 0.85, yheight]) # left, bottom, width, height    
    ax.set_title(subtitle, fontsize=12)

    # plot image
    array1 = np.array(analysis.as_matrix())

    #array aufteilen und mittelwerte berechnen TODO geht davon aus, dass anzahl proben gleich ist
    case =  array1[0:int(m/2)]
    control =  array1[int(m/2):m]
    counts = np.array(C.as_matrix())
    coverage = np.array(N.as_matrix())

    #array inklusive der Mittelwerte
    #array = np.vstack((case, np.mean(case, axis=0), np.mean(control, axis=0), control ))
    
    if show_results == True:
        assert results is not None
        result = np.array(results.as_matrix())
        print(result)
        array = np.vstack((case, np.sum(counts[0:int(m/2)], axis=0)/np.sum(coverage[0:int(m/2)], axis=0), result, np.sum(counts[int(m/2):m]/np.sum(coverage[int(m/2):m], axis=0)
        , axis=0), control ))

        coverage = np.vstack((coverage[0:int(m/2)], np.sum(coverage[0:int(m/2)], axis=0), np.sum(coverage[0:int(m/2)], axis=0), np.sum(coverage[int(m/2):m], axis=0), np.sum(coverage[int(m/2):m], axis=0),  coverage[int(m/2):m] ))
    else:
        array = np.vstack((case, np.sum(counts[0:int(m/2)], axis=0)/np.sum(coverage[0:int(m/2)], axis=0), np.sum(counts[int(m/2):m]/np.sum(coverage[int(m/2):m], axis=0)
        , axis=0), control ))
        coverage = np.vstack((coverage[0:int(m/2)], np.sum(coverage[0:int(m/2)], axis=0), np.sum(coverage[int(m/2):m], axis=0),  coverage[int(m/2):m] ))



    #coverage = np.vstack((coverage[0:int(m/2)], np.mean(coverage[0:int(m/2)], axis=0), np.mean(coverage[int(m/2):m], axis=0),  coverage[int(m/2):m] ))

    #image = ax.imshow(array, cmap=mycolormap,interpolation='none', origin='upper', vmin=0.0, vmax=1.0)
    image = ax.imshow(mycmap(array,coverage), 
        interpolation='none', origin='upper', vmin=0.0, vmax=1.0)
    ax.axhline(y=2.5,  color='black')
    ax.axhline(y=4.5,  color='black')
    if show_results == True:
        ax.axhline(y=6.5,  color='black')

    ax.set_aspect('auto')
    if show_results == True and n <=20:
        for i in range(m+4):
            for j in range(n):
                x = array[i,j]
                ax.text(j,i, "{:,.0%}".format(x), fontsize=8, color=fontcolor(x), ha='center')#Methylierung in %
                ax.text(j,i+0.2, "{:d}".format(coverage[i,j]), fontsize=8, color=fontcolor(x), ha='center')#Coverage
    else:
        if n<= 20:
            for i in range(m+2):
                for j in range(n):
                    x = array[i,j]
                    ax.text(j,i, "{:,.0%}".format(x), fontsize=8, color=fontcolor(x), ha='center')#Methylierung in %
                    ax.text(j,i+0.2, "{:d}".format(coverage[i,j]), fontsize=8, color=fontcolor(x), ha='center')#Coverage

    # column-wise methylation rates
    avgcolrates = np.mean(array1, axis=0)
    xfontsize = 8 if n < 20 else 6
    #x1labels = ["{:.0f}".format(100*m) for m in avgcolrates]
    x1labels = ["{:,.2f}".format(100*m) for m in avgcolrates]  
    #x2labels = ["{:d}".format(pos) for pos in cpgpos]
    x2labels = [".."+str(pos)[3:] for pos in cpgpos]
    xlabels = [x1+"\n"+x2 for x1,x2 in zip(x1labels,x2labels)]    
    ax.set_xlabel('Durchschnittliche Methylierungsraten [%] / CpG-Positionen')
    ax.set_xticks(range(n))
    ax.set_xticklabels(xlabels, fontsize=xfontsize)
    if show_results == True:
        y1labels = ["case1", "case2", "case3", "mean_case", "mu_i_case" , "mu_i_control" , "mean_control","control1", "control2","control3"]
        y2labels = [ "(Daten)","(Daten)","(Daten)","(Daten)","(errechnet)", "(errechnet)", "(Daten)","(Daten)","(Daten)","(Daten)"]
    else:
        y1labels = ["case1", "case2", "case3", "mean_case", "mean_control","control1", "control2","control3"]
        y2labels = [ "(Daten)" for _ in range(8)]
    #y2labels = [ "{:.1f} ({:d})".format(100 * 1, 100) for _ in range(6) ]
    ylabels = [y1+"\n"+y2 for y1,y2 in zip(y1labels,y2labels)]
    ax.set_yticks(range(m+4)) if show_results else ax.set_yticks(range(m+2))
    #ax.set_yticks(range(m+2))
    yfontsize = 8 if m < 21 else 6
    ax.set_yticklabels(ylabels, fontsize=yfontsize)

    # save to file
    if fname == "-":  fname = sys.stdout
    if show_results == True:
        fig.savefig(fname+"Vergleich.pdf", format=format)  # bbox_inches="tight" cuts off title!
    else:
        fig.savefig(fname+".pdf", format=format)  # bbox_inches="tight" cuts off title!
    plt.close(fig)
    fig = None
    return True


