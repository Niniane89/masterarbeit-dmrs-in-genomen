from operator import itemgetter
import numpy as np
import pandas as pd
import math
import os
from itertools import tee, chain, islice
from pprint import pprint
from array import array
import threading
import queue
from multiprocessing import Pool
import multiprocessing
from functools import partial
from concurrent.futures import ThreadPoolExecutor

print_out = False;
poolsize = 4;


def process(case, control, filename):
	"""
	case, control jeweils ein DataFrame, pro Probe jeweils eine Spalte "M" und "U+M", die Zeilen sind die 
	Positionen """
	#median_case: Klassenmethylierungswert aus dem Median der mu_i_t
	#mu_i_case: Klassenmethylierungswert aus den umliegenden Werten
	#mu_i_t_case: Methylierungswert pro Probe/ Position
	#_all: Dataframe aus allen bisherigen Iterationsergebnissen
	#Gewichtung der Iterationsschritte
	faktor = 0.5 #mu_i_t
	faktor_m = 0.5 #median
	faktor_class = 0.5
	#Gewichtungsfaktoren aus dem Optimierungsproblem
	a_meas= 2.0
	a_class = 31.37
	a_space_slope = 0.0024 # Steigung der Funktion ln(2)/b+ Abstand*m
	a_space_intercept = 0.06
	a_diff = 31.76
	#Skalierung der Gewichtungsfaktoren
	s_meas= 1.0 #Skalierungsfaktor fuer coverage
	s_class_1 = 1.0
	s_class = 1.0
	s_space = 1.0
	s_diff = 1.0
	# alles als params uebergeben (class Params: pass)
	
	epsilon =  10.0**(-6)

	iteration = 0  # Zaehler
	number_samples_case = case.shape[1]/2
	number_samples_control = control.shape[1]/2
	#assert not np.isnan(case).any()
	#assert not np.isnan(control).any()


	#Schritt 0 
	#initialisiere Klassenmethylierungswerte aus Messwerten
	mu_i_t_case, coverage_case = dezimal(case)
	mu_i_t_control, coverage_control = dezimal(control)

	#Eine Tabelle mit Gewichten a_space vorberechnen
	a_space_table = precompute_a_space(coverage_case.index, a_space_slope, a_space_intercept, s_space)
	print(a_space_table)


	store = pd.HDFStore("output/"+str(iteration)+".h5")
	#store['mu_i_case'] = mu_i_case
	#store['mu_i_control'] = mu_i_control
	#store['median_case'] = median_case
	#store['median_control'] = median_control
	store['mu_i_t_case'] = mu_i_t_case
	store['mu_i_t_control'] = mu_i_t_control

	print("Schritt1")
	#Schritt 1
	#Median ueber mu_i_t berechnen

	median_case = compute_median_mu_i_t(mu_i_t_case)
	median_control = compute_median_mu_i_t(mu_i_t_control)
	
	#pprint(median_case)
	print("Schritt3")
	#schritt 3

	#bei diesem schritt sind alle gleich dem median..
	#mu_i_t_case = compute_mu_i_t(median_case.loc[:,iteration], case, a_class*s_class_1, a_meas*s_meas)
	#mu_i_t_control = compute_mu_i_t(median_control.loc[:,iteration], control, a_class*s_class_1, a_meas*s_meas)
	#print(mu_i_t_case)

	#print(mu_i_t_case)
	print("Schritt2")

	#Schritt 2
	#Klassenmethylierungswerte aus den umliegenden Positionen
	
	mu_i_case, mu_i_control = compute_mu_class(median_case, median_control, mu_i_t_case, mu_i_t_control, a_space_table, a_diff*s_diff, a_class*s_class )
	mu_i_case.columns = [iteration]
	mu_i_control.columns = [iteration]
	print(mu_i_case)



	#ab hier mehrere Iterationen

	while(True):

		mu_i_case_old = mu_i_case.copy()
		
		mu_i_control_old = mu_i_control.copy()
		mu_i_t_case_old = mu_i_t_case.copy()
		mu_i_t_control_old = mu_i_t_control.copy()
		
		
		print(iteration)
		iteration = iteration +1

		print("vertikal optimieren")
		mu_i_case, mu_i_control, mu_i_t_case, mu_i_t_control = compute_v_x_plus_y(mu_i_case, mu_i_control,  mu_i_t_case, mu_i_t_control, case, control, a_space_table, a_diff*s_diff, a_class*s_class, a_meas*s_meas)

		mu_i_case, mu_i_control, mu_i_t_case, mu_i_t_control = compute_v_x_minus_y(mu_i_case, mu_i_control,  mu_i_t_case, mu_i_t_control, case, control, a_space_table, a_diff*s_diff, a_class*s_class, a_meas*s_meas)

		print("horizontal optimieren")
		print("x+y optimieren")
		mu_i_case, mu_i_control = compute_t_x_plus_y(mu_i_case, mu_i_control, mu_i_t_case, mu_i_t_control, a_space_table, a_diff*s_diff, a_class*s_class )
		print("x-y optimieren")
		mu_i_case, mu_i_control = compute_t_x_minus_y(mu_i_case, mu_i_control, mu_i_t_case, mu_i_t_control, a_space_table, a_diff*s_diff, a_class*s_class )


		
		#Schritt 1
		#print("Schritt1")
		#Median ueber mu_i_t berechnen
		#mu_i_case = compute_median_mu_i_t(mu_i_t_case)
		#mu_i_control = compute_median_mu_i_t(mu_i_t_control)

		#glaetten
	
		#mu_i_case = pd.DataFrame(mu_i_case_old.loc[:,iteration-1].add(faktor_m*(mu_i_case.loc[:,0].sub(mu_i_case_old.loc[:,iteration-1]))))
		#mu_i_control = pd.DataFrame(mu_i_control_old.loc[:,iteration-1].add(faktor_m*(mu_i_control.loc[:, 0].sub(mu_i_control_old.loc[:,iteration-1]))))
		
		#Spaltennamen hinzufuegen
		mu_i_case.columns=[iteration]
		mu_i_control.columns=[iteration]

		#Schritt 3
		print("Schritt3")
		#mu_i_t berechnen aus Messwerten und dem Klassenmethylierungswert
		#mapfunc_control = partial(compute_mu_i_t, median_control.loc[:,iteration], control, a_class*s_class, a_meas*s_meas)

		executor = ThreadPoolExecutor(max_workers=2)
		a = executor.submit(compute_mu_i_t, mu_i_case.loc[:,iteration], case,  a_class*s_class_1, a_meas*s_meas)
		b = executor.submit(compute_mu_i_t, mu_i_control.loc[:,iteration], control, a_class*s_class_1, a_meas*s_meas)
		
		mu_i_t_case = a.result()
		mu_i_t_control = b.result()

		#mu_i_t_case = compute_mu_i_t(median_case.loc[:,iteration], case,  a_class*s_class, a_meas*s_meas)		
		#mu_i_t_control = compute_mu_i_t(median_control.loc[:,iteration], control, a_class*s_class, a_meas*s_meas)
		
		#glaetten
		mu_i_t_case = glaetten(mu_i_t_case_old, mu_i_t_case, faktor, iteration, number_samples_case)
		mu_i_t_control = glaetten(mu_i_t_control_old, mu_i_t_control, faktor, iteration, number_samples_control)



		#Schritt 2
		print("Schritt2")
		#Klassenmethylierungswert mit umliegenden Werten
		mu_i_case, mu_i_control = compute_mu_class(mu_i_case, mu_i_control, mu_i_t_case, mu_i_t_control, a_space_table, a_diff*s_diff, a_class*s_class )

		#glaetten
		mu_i_case = glaetten(mu_i_case_old, mu_i_case, faktor_class, iteration)
		mu_i_control = glaetten(mu_i_control_old, mu_i_control, faktor_class, iteration)

		#Spaltennamen hinzufuegen
		mu_i_case.columns=[iteration]
		mu_i_control.columns=[iteration]
		print(mu_i_case)
		print(mu_i_control)
	
		if(print_out):
			outfolder = "output/"
			median_control.to_csv(outfolder + "median_control_"+str(iteration)+".csv", sep= "\t")
			mu_i_case.to_csv(outfolder +"mu_i_case_"+str(iteration)+".csv", sep= "\t")
			mu_i_control.to_csv(outfolder +"mu_i_control_"+str(iteration)+".csv", sep= "\t")
			mu_i_t_case.to_csv(outfolder +"mu_i_t_case_"+str(iteration)+".csv", sep= "\t")
			mu_i_t_control.to_csv(outfolder +"mu_i_t_control_"+str(iteration)+".csv", sep= "\t")
			median_case.to_csv(outfolder +"median_case_"+str(iteration)+".csv", sep= "\t")

	
		#Vergleich, evtl alternierend
	
		
		
		dist2 = mu_i_case.loc[:, iteration].sub(mu_i_case_old.loc[:,iteration-1])
		dist2 = dist2[abs(dist2) > epsilon]
		dist1 = mu_i_control.loc[:, iteration].sub(mu_i_control_old.loc[:,iteration-1])
		dist1 = dist1[abs(dist1) > epsilon]
		print(dist1)
		if(dist1.empty and dist2.empty):
			break
		if (iteration> 50):
			break
	
	outfolder = "output/"
	store = pd.HDFStore(outfolder+str(iteration)+filename+".h5")
	store['mu_i_case'] = mu_i_case
	store['mu_i_control'] = mu_i_control
	store['median_case'] = median_case
	store['median_control'] = median_control
	store['mu_i_t_case'] = mu_i_t_case
	store['mu_i_t_control'] = mu_i_t_control	

	

def precompute_a_space(index, m, b, s_space):
	'''berechnet eine Tabelle mit Gewichten a_space vor. a_space_table[index] = Gewichtung zum naechsthoeheren Index'''
	a_space_table = pd.Series(index = index)
	for x, y in pairwise(index):
		a_space_table[x] = np.round(s_space * a_space_func(y-x, m, b) , 2)
	return a_space_table




def a_space_func(x, m, b):
	if x > 200:
		return 0
	return (math.log(2)/(x*m+b))



def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)


def glaetten(values_all, values, faktor, iteration, number_samples = 1):
	"""number_samples Anzahl Proben"""





	#a = values_all.iloc[:,((iteration-1)*number_samples): (iteration*number_samples)]
	a = values_all.iloc[:,0:number_samples]
	b = values.iloc[:,0:number_samples]
	if number_samples == 1:
		a = values_all.loc[:,iteration-1]
		b = values.iloc[:,0]
	result = pd.DataFrame(a.add(faktor*(b.sub(a, axis = 0)) ))

	return result





def t_x_plus_y_muit_params_generator(v, start, stop, step, mu_i_group, mu_i_opposite_group, mu_i_t_group, group_sample, a_space_table, a_diff, a_class, a_meas ):
	coverage_group = group_sample.loc[:,"U+M"]
	M = group_sample.loc[:,"M"]
	values_group = M / coverage_group.values[:,:]

	for i in range(start, stop, step):
		weights = []
		#Nachbarn links und rechts von t
		#Grenzen abfragen
		if i > 0:
			weights.append(( mu_i_group.iloc[i,0] - mu_i_group.iloc[i-1,0],  a_space_table.iloc[i-1]))
		if i < mu_i_group.shape[0]-1:
			weights.append((mu_i_group.iloc[i,0] - mu_i_group.iloc[i+1,0], a_space_table.iloc[i]))
		#andere Gruppe
		weights.append(( mu_i_group.iloc[i,0] - mu_i_opposite_group.iloc[i, 0], a_diff))
		
		#zugehoerige mu_i_t
		for column in mu_i_t_group.columns:
			if column == v:
				weights.append(( values_group.iloc[i, column] - mu_i_t_group.iloc[i, column], a_meas*coverage_group.iloc[i, column]))
			#else:
				#weights.append(( mu_i_group.iloc[i,0] - mu_i_t_group.iloc[i, column], a_class ))
		yield weights


def t_x_minus_y_muit_params_generator(v, start, stop, step, mu_i_group, mu_i_opposite_group, mu_i_t_group, group_sample, a_space_table, a_diff, a_class, a_meas ):
	coverage_group = group_sample.loc[:,"U+M"]
	M = group_sample.loc[:,"M"]
	values_group = M / coverage_group.values[:,:]

	for i in range(start, stop, step):
		
		weights = []
		#Nachbarn links und rechts von t

		#Grenzen abfragen
		if i > 0:
			weights.append(( mu_i_group.iloc[i-1,0]- mu_i_group.iloc[i,0],  a_space_table.iloc[i-1]))
		if i < mu_i_group.shape[0]-1:
			weights.append((mu_i_group.iloc[i+1,0] - mu_i_group.iloc[i,0], a_space_table.iloc[i]))

		#andere Gruppe
		weights.append(( mu_i_opposite_group.iloc[i, 0]- mu_i_group.iloc[i,0], a_diff))
		#zugehoerige mu_i_t
		for column in mu_i_t_group.columns:
			if column == v:
				weights.append(( values_group.iloc[i, column] - mu_i_t_group.iloc[i, column], a_meas*coverage_group.iloc[i, column]))
			#else:
				#weights.append(( mu_i_t_group.iloc[i, column] - mu_i_group.iloc[i,0], a_class ))
		yield weights


def compute_v_x_plus_y(mu_i_case, mu_i_control,  mu_i_t_case, mu_i_t_control, case, control, a_space_table, a_diff, a_class, a_meas):
	"""Fuer jede Position wird der Klassenmethylierungswert anhand der umliegenden Klassenmethylierungswerte,
	sowie der zugehörigen mu_i_t neu berechnet
	mu_i_group: DataFrame der einen Gruppe (case)
	mu_i_opposite_group: DataFrame der anderen Gruppe (control)
	mu_i_t_group: DataFrame, der fuer jede Position der Klasse die zuvor berechneten Methylierungswerte fuer die Proben enthält
	a_space_table Series, Eintrag [index] enthält vorberechnete Gewichtung für den Abstand zum Nachbarn [index+1]
	a_diff Gewicht fuer andere Gruppe, gleiche Position
	a_class Gewichtung fuer Methylierungswerte mu_i_t der Position

	"""

	
	N = mu_i_case.shape[0]

	values_case , coverage_case = dezimal(case)
	values_control, coverage_control = dezimal(control)
	#print(mu_i_t_case)



	for v in mu_i_t_case.columns:
		for i in range(0, N-1):
			weights_case = []
			#Nachbarn links und rechts von t
			#Grenzen abfragen
			if i > 0:
				weights_case.append(( mu_i_case.iloc[i,0] - mu_i_case.iloc[i-1,0],  a_space_table.iloc[i-1]))
			if i < mu_i_case.shape[0]-1:
				weights_case.append((mu_i_case.iloc[i,0] - mu_i_case.iloc[i+1,0], a_space_table.iloc[i]))
			#andere Gruppe
			weights_case.append(( mu_i_case.iloc[i,0] - mu_i_control.iloc[i, 0], a_diff))

			#zugehoerige mu_i_t
			
			for column in mu_i_t_case.columns:
				if column == v:
					weights_case.append(( values_case.iloc[i, column] - mu_i_t_case.iloc[i, column], a_meas*coverage_case.iloc[i, column]))
				else:
					weights_case.append(( mu_i_case.iloc[i,0] - mu_i_t_case.iloc[i, column], a_class ))

			t1 = minimize_weighted_absolute_distance(weights_case)

			mu_i_case.iloc[i] = 1 if (mu_i_case.iloc[i].values - t1 > 1) else (0 if mu_i_case.iloc[i].values - t1 < 0 else mu_i_case.iloc[i] - t1)
			mu_i_t_case.iloc[i, v] = 1 if (mu_i_t_case.iloc[i, v] + t1 > 1) else (0 if mu_i_t_case.iloc[i, v] + t1 < 0 else mu_i_t_case.iloc[i, v] + t1)
				

	for v in mu_i_t_control.columns:
		for i in range(0, N-1):
			weights_control = []
			#Nachbarn links und rechts von t
			#Grenzen abfragen
			if i > 0:
				weights_control.append(( mu_i_control.iloc[i,0] - mu_i_control.iloc[i-1,0],  a_space_table.iloc[i-1]))
			if i < N-1:
				weights_control.append((mu_i_control.iloc[i,0] - mu_i_control.iloc[i+1,0], a_space_table.iloc[i]))
			#andere Gruppe
			weights_control.append(( mu_i_control.iloc[i,0] - mu_i_case.iloc[i, 0], a_diff))
			#zugehoerige mu_i_t
			for column in mu_i_t_case.columns:
				if column == v:
					weights_control.append(( values_control.iloc[i, column] - mu_i_t_control.iloc[i, column], a_meas*coverage_control.iloc[i, column]))
				else:
					weights_control.append(( mu_i_control.iloc[i,0] - mu_i_t_control.iloc[i, column], a_class ))
			t2 = minimize_weighted_absolute_distance(weights_control)
		
			mu_i_control.iloc[i] = 1 if mu_i_control.iloc[i].values - t2 > 1 else (0 if mu_i_control.iloc[i].values - t2 < 0 else mu_i_control.iloc[i] - t2)
			mu_i_t_control.iloc[i, v] = 1 if mu_i_t_control.iloc[i, v] + t2 > 1 else (0 if mu_i_t_control.iloc[i, v] + t2 < 0 else mu_i_t_control.iloc[i, v] + t2)
	


	return mu_i_case, mu_i_control, mu_i_t_case, mu_i_t_control


def compute_v_x_minus_y(mu_i_case, mu_i_control,  mu_i_t_case, mu_i_t_control, case, control, a_space_table, a_diff, a_class, a_meas):
	"""Fuer jede Position wird der Klassenmethylierungswert anhand der umliegenden Klassenmethylierungswerte,
	sowie der zugehörigen mu_i_t neu berechnet
	mu_i_group: DataFrame der einen Gruppe (case)
	mu_i_opposite_group: DataFrame der anderen Gruppe (control)
	mu_i_t_group: DataFrame, der fuer jede Position der Klasse die zuvor berechneten Methylierungswerte fuer die Proben enthält
	a_space_table Series, Eintrag [index] enthält vorberechnete Gewichtung für den Abstand zum Nachbarn [index+1]
	a_diff Gewicht fuer andere Gruppe, gleiche Position
	a_class Gewichtung fuer Methylierungswerte mu_i_t der Position
	"""

	N = mu_i_case.shape[0]

	values_case , coverage_case = dezimal(case)
	values_control, coverage_control = dezimal(control)

	for v in mu_i_t_case.columns:
		for i in range(1, N):
			weights_case = []
			#Nachbarn links und rechts von t
			#Grenzen abfragen
			weights_case.append((mu_i_case.iloc[i-1,0] - mu_i_case.iloc[i,0],  a_space_table.iloc[i-1]))
			if i < mu_i_case.shape[0]-1:
				weights_case.append((mu_i_case.iloc[i+1,0] - mu_i_case.iloc[i,0], a_space_table.iloc[i]))
			#andere Gruppe
			weights_case.append((mu_i_control.iloc[i, 0] - mu_i_case.iloc[i,0], a_diff))
			#zugehoerige mu_i_t
			for column in mu_i_t_case.columns:
				if column == v:
					#messwert
					weights_case.append(( values_case.iloc[i, column] - mu_i_t_case.iloc[i, column], a_meas*coverage_case.iloc[i, column]))
				else:
					weights_case.append(( mu_i_t_case.iloc[i, column] -mu_i_case.iloc[i,0], a_class ))
			t1 = minimize_weighted_absolute_distance(weights_case)
			mu_i_case.iloc[i] = 1 if (mu_i_case.iloc[i].values + t1 > 1) else (0 if mu_i_case.iloc[i].values + t1 < 0 else mu_i_case.iloc[i] + t1)
			mu_i_t_case.iloc[i, v] = 1 if (mu_i_t_case.iloc[i, v] + t1 > 1) else (0 if mu_i_t_case.iloc[i, v] + t1 < 0 else mu_i_t_case.iloc[i, v] + t1)
				

	for v in mu_i_t_control.columns:
		for i in range(1, N):
			weights_control = []
			#Nachbarn links und rechts von t
			#Grenzen abfragen
			if i > 0:
				weights_control.append(( mu_i_control.iloc[i-1,0] - mu_i_control.iloc[i,0],  a_space_table.iloc[i-1]))
			if i < N-1:
				weights_control.append((mu_i_control.iloc[i+1,0] - mu_i_control.iloc[i,0], a_space_table.iloc[i]))
			#andere Gruppe
			weights_control.append((  mu_i_case.iloc[i, 0] - mu_i_control.iloc[i,0], a_diff))
			#zugehoerige mu_i_t
		
			for column in mu_i_t_case.columns:
				if column == v:
					weights_control.append(( values_control.iloc[i, column] - mu_i_t_control.iloc[i, column], a_meas*coverage_control.iloc[i, column]))
				else:
					weights_control.append((mu_i_t_control.iloc[i, column] - mu_i_control.iloc[i,0], a_class ))
			t2 = minimize_weighted_absolute_distance(weights_control)
		
			mu_i_control.iloc[i] = 1 if mu_i_control.iloc[i].values + t2 > 1 else (0 if mu_i_control.iloc[i].values + t2 < 0 else mu_i_control.iloc[i] + t2)
			mu_i_t_control.iloc[i, v] = 1 if mu_i_t_control.iloc[i, v] + t2 > 1 else (0 if mu_i_t_control.iloc[i, v] + t2 < 0 else mu_i_t_control.iloc[i, v] + t2)
	


	return mu_i_case, mu_i_control, mu_i_t_case, mu_i_t_control


def compute_v(mu_i_case, mu_i_control,  mu_i_t_case, mu_i_t_control, case, control, a_space_table, a_diff, a_class, a_meas , typ="x-y"):
	"""Fuer jede Position wird der Klassenmethylierungswert anhand der umliegenden Klassenmethylierungswerte,
	sowie der zugehörigen mu_i_t neu berechnet
	mu_i_group: DataFrame der einen Gruppe (case)
	mu_i_opposite_group: DataFrame der anderen Gruppe (control)
	mu_i_t_group: DataFrame, der fuer jede Position der Klasse die zuvor berechneten Methylierungswerte fuer die Proben enthält
	a_space_table Series, Eintrag [index] enthält vorberechnete Gewichtung für den Abstand zum Nachbarn [index+1]
	a_diff Gewicht fuer andere Gruppe, gleiche Position
	a_class Gewichtung fuer Methylierungswerte mu_i_t der Position

	"""

	
	N = mu_i_case.shape[0]
	#print(mu_i_t_case)

	for v in mu_i_t_case.columns:
		if typ == "x-y":
			mymap_case = map(minimize_weighted_absolute_distance, t_x_minus_y_muit_params_generator(v, 0, N-1, 1,  mu_i_case, mu_i_control, mu_i_t_case, case, a_space_table, a_diff, a_class, a_meas ))
			mymap_control = map(minimize_weighted_absolute_distance, t_x_minus_y_muit_params_generator(v, 0, N-1, 1,  mu_i_control, mu_i_case, mu_i_t_control, control, a_space_table, a_diff, a_class, a_meas ))

		else:
			mymap_case = map(minimize_weighted_absolute_distance, t_x_plus_y_muit_params_generator(v, 0, N-1, 1,  mu_i_case, mu_i_control, mu_i_t_case, case, a_space_table, a_diff, a_class, a_meas ))
			mymap_control = map(minimize_weighted_absolute_distance, t_x_plus_y_muit_params_generator(v, 0, N-1, 1,  mu_i_control, mu_i_case, mu_i_t_control, control, a_space_table, a_diff, a_class, a_meas ))

	    # generate and report results using the appropriate map object
		i = 0

		for t1, t2  in zip(mymap_case, mymap_control):
			#print([t1, t2])
			if typ == "x+y":
				
				mu_i_case.iloc[i] = 1 if (mu_i_case.iloc[i].values + t1 > 1) else (0 if mu_i_case.iloc[i].values + t1 < 0 else mu_i_case.iloc[i] + t1)
				mu_i_control.iloc[i] = 1 if mu_i_control.iloc[i].values + t2 > 1 else (0 if mu_i_control.iloc[i].values + t2 < 0 else mu_i_control.iloc[i] + t2)
			else:
				mu_i_case.iloc[i] = 1 if (mu_i_case.iloc[i].values - t1 > 1) else (0 if mu_i_case.iloc[i].values - t1 < 0 else mu_i_case.iloc[i] - t1)
				mu_i_control.iloc[i] = 1 if mu_i_control.iloc[i].values - t2 > 1 else (0 if mu_i_control.iloc[i].values - t2 < 0 else mu_i_control.iloc[i] - t2)

			mu_i_t_case.iloc[i, v] = 1 if (mu_i_t_case.iloc[i, v] + t1 > 1) else (0 if mu_i_t_case.iloc[i, v] + t1 < 0 else mu_i_t_case.iloc[i, v] + t1)
			mu_i_t_control.iloc[i, v] = 1 if mu_i_t_control.iloc[i, v] + t2 > 1 else (0 if mu_i_t_control.iloc[i, v] + t2 < 0 else mu_i_t_control.iloc[i, v] + t2)
			i = i+1


	return mu_i_case, mu_i_control, mu_i_t_case, mu_i_t_control







def compute_t_x_minus_y(mu_i_case, mu_i_control,  mu_i_t_case, mu_i_t_control, a_space_table, a_diff, a_class ):
	"""Fuer jede Position wird der Klassenmethylierungswert anhand der umliegenden Klassenmethylierungswerte,
	sowie der zugehörigen mu_i_t neu berechnet
	mu_i_group: DataFrame der einen Gruppe (case)
	mu_i_opposite_group: DataFrame der anderen Gruppe (control)
	mu_i_t_group: DataFrame, der fuer jede Position der Klasse die zuvor berechneten Methylierungswerte fuer die Proben enthält
	a_space_table Series, Eintrag [index] enthält vorberechnete Gewichtung für den Abstand zum Nachbarn [index+1]
	a_diff Gewicht fuer andere Gruppe, gleiche Position
	a_class Gewichtung fuer Methylierungswerte mu_i_t der Position

	i-1 ist x, i y"""


	N = mu_i_case.shape[0]
	for i in range(1, N-1):
		weights_case = []
		weights_control = []
		#Nachbarn links und rechts von t
		#Grenzen abfragen
		if i > 1:

			weights_case.append(( mu_i_case.iloc[i-2,0]- mu_i_case.iloc[i-1,0],  a_space_table.iloc[i-2]))
			weights_control.append(( mu_i_control.iloc[i-2,0]- mu_i_control.iloc[i-1,0],  a_space_table.iloc[i-2]))
		if i < N-1:
			weights_case.append(( mu_i_case.iloc[i+1,0]-mu_i_case.iloc[i,0], a_space_table.iloc[i]))
			weights_control.append(( mu_i_control.iloc[i+1,0]-mu_i_control.iloc[i,0], a_space_table.iloc[i]))

		#andere Gruppe
		#case
		weights_case.append(( mu_i_control.iloc[i-1, 0]- mu_i_case.iloc[i-1,0], a_diff))
		weights_case.append(( mu_i_control.iloc[i, 0]- mu_i_case.iloc[i,0], a_diff))
		#control
		weights_control.append(( mu_i_case.iloc[i-1, 0]- mu_i_control.iloc[i-1,0], a_diff))
		weights_control.append(( mu_i_case.iloc[i, 0]- mu_i_control.iloc[i,0], a_diff))
		#zugehoerige mu_i_t
		for column in mu_i_t_case.columns:
			weights_case.append(( mu_i_t_case.iloc[i, column] - mu_i_case.iloc[i,0], a_class ))
			weights_case.append(( mu_i_t_case.iloc[i-1, column] - mu_i_case.iloc[i-1,0], a_class ))
		for column in mu_i_t_control.columns:
			weights_control.append(( mu_i_t_control.iloc[i, column] - mu_i_control.iloc[i,0], a_class ))
			weights_control.append(( mu_i_t_control.iloc[i-1, column] - mu_i_control.iloc[i-1,0], a_class ))
		
		#print(weights_case)

		t1 = minimize_weighted_absolute_distance(weights_case)
		t2 = minimize_weighted_absolute_distance(weights_control)
	

		#wert zwischen 0 und 1
		mu_i_case.iloc[i-1] = 1 if (mu_i_case.iloc[i-1].values + t1 > 1) else (0 if mu_i_case.iloc[i-1].values + t1 < 0 else mu_i_case.iloc[i-1] + t1)
		mu_i_case.iloc[i] =  1 if (mu_i_case.iloc[i].values - t1 > 1) else (0 if mu_i_case.iloc[i].values - t1 < 0 else mu_i_case.iloc[i] + t1)
		mu_i_control.iloc[i-1] = 1 if (mu_i_control.iloc[i-1].values + t2 > 1) else (0 if mu_i_control.iloc[i-1].values + t2 < 0 else mu_i_control.iloc[i-1] + t2)
		mu_i_control.iloc[i] = 1 if (mu_i_control.iloc[i].values - t2 > 1) else (0 if mu_i_control.iloc[i].values - t2 < 0 else mu_i_control.iloc[i] + t2)


	return mu_i_case, mu_i_control




def compute_t_x_plus_y(mu_i_case, mu_i_control,  mu_i_t_case, mu_i_t_control, a_space_table, a_diff, a_class ):
	"""Fuer jede Position wird der Klassenmethylierungswert anhand der umliegenden Klassenmethylierungswerte,
	sowie der zugehörigen mu_i_t neu berechnet
	mu_i_group: DataFrame der einen Gruppe (case)
	mu_i_opposite_group: DataFrame der anderen Gruppe (control)
	mu_i_t_group: DataFrame, der fuer jede Position der Klasse die zuvor berechneten Methylierungswerte fuer die Proben enthält
	a_space_table Series, Eintrag [index] enthält vorberechnete Gewichtung für den Abstand zum Nachbarn [index+1]
	a_diff Gewicht fuer andere Gruppe, gleiche Position
	a_class Gewichtung fuer Methylierungswerte mu_i_t der Position

	i-1 ist x, i y"""


	N = mu_i_case.shape[0]
	for i in range(1, N-1):
		weights_case = []
		weights_control = []
		#Nachbarn links und rechts von t
		#Grenzen abfragen
		if i > 1:
			weights_case.append(( mu_i_case.iloc[i-2,0]- mu_i_case.iloc[i-1,0],  a_space_table.iloc[i-2]))
			weights_control.append(( mu_i_control.iloc[i-2,0]- mu_i_control.iloc[i-1,0],  a_space_table.iloc[i-2]))
		if i < N-1:
			weights_case.append(( mu_i_case.iloc[i,0]-mu_i_case.iloc[i+1,0], a_space_table.iloc[i]))
			weights_control.append(( mu_i_control.iloc[i,0]-mu_i_control.iloc[i+1,0], a_space_table.iloc[i]))

		#andere Gruppe
		#case
		weights_case.append(( mu_i_control.iloc[i-1, 0]- mu_i_case.iloc[i-1,0], a_diff))
		weights_case.append(( mu_i_case.iloc[i, 0]- mu_i_control.iloc[i,0], a_diff))  #umgedreht

	#control
		weights_control.append(( mu_i_case.iloc[i-1, 0]- mu_i_control.iloc[i-1,0], a_diff))
		weights_control.append(( mu_i_control.iloc[i, 0]- mu_i_case.iloc[i,0], a_diff))
		#zugehoerige mu_i_t
		for column in mu_i_t_case.columns:
			weights_case.append(( mu_i_t_case.iloc[i, column] - mu_i_case.iloc[i,0], a_class ))
			weights_case.append(( mu_i_t_case.iloc[i-1, column] - mu_i_case.iloc[i-1,0], a_class ))
		for column in mu_i_t_control.columns:
			weights_control.append(( mu_i_t_control.iloc[i, column] - mu_i_control.iloc[i,0], a_class ))
			weights_control.append(( mu_i_t_control.iloc[i-1, column] - mu_i_control.iloc[i-1,0], a_class ))
		
		t1 = minimize_weighted_absolute_distance(weights_case)
		t2 = minimize_weighted_absolute_distance(weights_control)


		#wert zwischen 0 und 1
		mu_i_case.iloc[i-1] = 1 if (mu_i_case.iloc[i-1].values + t1 > 1) else (0 if mu_i_case.iloc[i-1].values + t1 < 0 else mu_i_case.iloc[i-1] + t1)
		mu_i_case.iloc[i] =  1 if (mu_i_case.iloc[i].values - t1 > 1) else (0 if mu_i_case.iloc[i].values - t1 < 0 else mu_i_case.iloc[i] - t1)
		mu_i_control.iloc[i-1] = 1 if (mu_i_control.iloc[i-1].values + t2 > 1) else (0 if mu_i_control.iloc[i-1].values + t2 < 0 else mu_i_control.iloc[i-1] + t2)
		mu_i_control.iloc[i] = 1 if (mu_i_control.iloc[i].values - t2 > 1) else (0 if mu_i_control.iloc[i].values - t2 < 0 else mu_i_control.iloc[i] - t2)


	return mu_i_case, mu_i_control


def mu_i_class_params_generator(start, stop, step, mu_i_group, mu_i_opposite_group, mu_i_t_group, a_space_table, a_diff, a_class ):
	for i in range(start, stop, step):
		weights = []
		#linker und rechter Nachbar
		#Grenzen abfragen
		if i > 0:
			weights.append(( mu_i_group.iloc[i-1,0],  a_space_table.iloc[i-1]))
		if i < mu_i_group.shape[0]-1:
			weights.append(( mu_i_group.iloc[i+1,0], a_space_table.iloc[i]))
		#andere Gruppe
		weights.append(( mu_i_opposite_group.iloc[i, 0], a_diff))
		#zugehoerige mu_i_t
		for column in mu_i_t_group.columns:
			weights.append(( mu_i_t_group.iloc[i, column], a_class ))
		#return minimize_weighted_absolute_distance(weights)
		yield weights




def compute_mu_class(mu_i_case, mu_i_control,  mu_i_t_case, mu_i_t_control, a_space_table, a_diff, a_class):
	"""Fuer jede Position wird der Klassenmethylierungswert anhand der umliegenden Klassenmethylierungswerte,
	sowie der zugehörigen mu_i_t neu berechnet
	mu_i_group: DataFrame der einen Gruppe (case)
	mu_i_opposite_group: DataFrame der anderen Gruppe (control)
	mu_i_t_group: DataFrame, der fuer jede Position der Klasse die zuvor berechneten Methylierungswerte fuer die Proben enthält
	a_space_table Series, Eintrag [index] enthält vorberechnete Gewichtung für den Abstand zum Nachbarn [index+1]
	a_diff Gewicht fuer andere Gruppe, gleiche Position
	a_class Gewichtung fuer Methylierungswerte mu_i_t der Position"""
	new_mu_i_case = pd.DataFrame(index = mu_i_case.index, columns = ["mu_i_case"])
	new_mu_i_control = pd.DataFrame(index = mu_i_control.index, columns = [ "mu_i_control"])
	N = mu_i_case.shape[0]


	for start_pos in range(2):
		start_pos_control = 1 if start_pos == 0 else 0
		if poolsize > 1:
			# analyze in parallel
			mypool = multiprocessing.Pool(poolsize)
			
			mymap_case = mypool.imap(minimize_weighted_absolute_distance, mu_i_class_params_generator(start_pos, N, 2,  mu_i_case, mu_i_control, mu_i_t_case, a_space_table, a_diff, a_class ), chunksize=500)
			mymap_control = mypool.imap(minimize_weighted_absolute_distance, mu_i_class_params_generator(start_pos_control, N, 2,  mu_i_control, mu_i_case, mu_i_t_control, a_space_table, a_diff, a_class ), chunksize=500)
			
		else:
			# analyze sequentially
			#print(clock.toc(), "analyzing reads sequentially...", file=fmsg)
			mymap_case = map(minimize_weighted_absolute_distance, mu_i_class_params_generator(start_pos, N, 2,  mu_i_case, mu_i_control, mu_i_t_case, a_space_table, a_diff, a_class))
			start_pos_control = 1 if start_pos == 0 else 0
			mymap_control = map(minimize_weighted_absolute_distance, mu_i_class_params_generator(start_pos_control, N, 2,  mu_i_control, mu_i_case, mu_i_t_control, a_space_table, a_diff, a_class ))

	    # generate and report results using the appropriate map object
		i = start_pos
		j = start_pos_control
		for result in mymap_case:
			new_mu_i_case.iloc[i] = result
			i = i+2
		for result in mymap_control:
			new_mu_i_control.iloc[j] = result
			j = j+2

		# done
		if poolsize > 1:  mypool.close();  mypool.join()
		#print(clock.toc(),"done", file=fmsg)

	return new_mu_i_case, new_mu_i_control










def minimize_weighted_absolute_distance(weights):
	"""input: einen Vektor von Tupeln aus Faktoren a und Gewichten w: [(a1,w1), (a2, w2),..]
	Output: Das Minimum der Summe w_i|x-a_i|, also mu_i_class"""
	#Gewichte aufaddieren
	w_sum = sum(elem[1] for elem in weights)
	#todo: was wenn size 0 ist??
	#Nach den a sortieren
	data = sorted(weights)

	#durchgehen und die Gewichte aufsummieren bis sie größer sind als Anzahl Elemente/2
	summe = 0
	T = w_sum/2
	for elem in data:
		summe = summe + elem[1]
		if summe >= T:
			return elem[0]
			#todo: hier geht die Information M/ (M+U ) verloren!!!





def compute_mu_i_t(mu_i_class, group_data, w_class, w_data):
	"""mu_i_class ist nx1 Matrix aus Methylierungswerten,
	 groupdata ist die Matrix der Messwerte, fuer jede Proebe eine Spalte M, U+M
	 result: DataFrame mit je einer Spalte pro Probe der entsprechenden Gruppe, enthaelt mu_i_t fuer alle i, t"""
	result = pd.DataFrame(index = mu_i_class.index, columns = [r for r in range(int(group_data.shape[1]/2))])
	
	values, coverage = dezimal(group_data)

	#alle Zeilen durchgehen
	for ind in mu_i_class.index:
		#jede Position, aber alle Proben
		clas = mu_i_class.loc[ind]
		for column in values.columns:
			res = minimize_weighted_absolute_and_squared_distance(w_class, clas, w_data*coverage.loc[ind, column], values.loc[ind, column])
			result.loc[ind, column] = np.round(res, 4)

	return result




def minimize_weighted_absolute_and_squared_distance(w1, a1, w2, a2):
	"""Input: Tupel aus Gewicht w1,2 und a1,a2 wobei w1,a1 fuer den Betrag sind"""
	#rechter Term faellt weg
	if w2 <= 0:
		return a1
	#x_r falls x >= a1
	x_r = a2 - (w1/(2*w2))
	if x_r >= a1:
		return x_r
	#x_l falls x <= a1
	x_l = a2 + (w1/(2*w2))
	if x_l <= a1:
		return x_l

	#Es gibt kein globales Minimum, sondern es liegt an den Grenzen
	#Fall 1: x = a1
	f_x_1 = w2*(a1-a2)**2
	#Fall 2: x = a2
	f_x_2 = w1 * abs(a2-a1)
	if (f_x_1 < f_x_2):
		return a1	
	else:
		return a2



	

def compute_median_mu_i_t(groupdata):
	#todo params weg und wegen nan gucken
	median = groupdata.median(axis = 1)
	median = pd.DataFrame(median)

	return median 



def compute_weighted_median_mu_i_t(groupdata, coverage,n):
	"""Coverage enthaelt fuer jede Probe die coverage
	Die Spalten in groupdata sind von 0 bis n-1 nummeriert"""
	result = pd.DataFrame(index =groupdata.index, columns = [0])
	for ind, row in groupdata.iterrows():
		values = []
		#alle spalten durchgehen
		for i in range(n):
			for _ in range( int(coverage.loc[ind,i])):
				values.append(row[i])
		if values:
			result.loc[ind] = np.median(values)
		else:
			#todo: wenn die coverage 0 ist, ist dann der wert 0?
			result.loc[ind] = 0
	return result
		




def initialize_mu_i_t(groupdata):
	#TODO: nicht durch 0 teilen!
	#Zeilen durchgehen bis auf erste und Median ueber Zeile bilden

	M = groupdata.loc[:,"M"]
	M_plus_U = groupdata.loc[:,"U+M"]
	result = M / M_plus_U.values[:,:]  # .values ist notwendig, k.A. warum.
	result.columns = np.arange(len(result.columns))
	result = result.replace([np.inf, -np.inf], np.nan)
	result.fillna(0, inplace = True)

	#M_plus_U.columns = np.arange(len(result.columns))

	return result

	#dezimal = lambda row: row[0]/row[1]
def dezimal(df):
	coverage = df.loc[:,"U+M"]
	M = df.loc[:,"M"]
	values = M / coverage.values[:,:]
	values.columns = np.arange(len(values.columns))
	coverage.columns = np.arange(len(coverage.columns))
	values = values.replace([np.inf, -np.inf], np.nan)
	values.fillna(0, inplace = True)
	values = np.round(values, decimals=2)
	return values, coverage




def read_Macaque_with_Pandas(filename):
	""" Lies .bed Datei im Format CpG-Startposition Methylierungswert ein
	Ausgabe: DataFrame im Format CpG-Startposition Methylierungswerte
	filenames: Liste von Tupeln aus directory und filename"""
	frame = pd.DataFrame()
	dfs = []
	print(filename)
	frame = pd.read_csv(filename, delim_whitespace = True, index_col = 0)
	frame.columns = ["M","U+M","M","U+M","M","U+M","M","U+M","M","U+M","M","U+M"]
	case = frame.iloc[:,[0,1,6,7,10,11]]
	control= frame.iloc[:,[2,3,4,5,8,9]]
	pprint(frame.shape)
	return case, control, frame






if __name__ == "__main__":
	folder=""#Ordner zu den Daten
	for filename in os.listdir(folder):
		case , control, _ = read_Macaque_with_Pandas(folder+filename)
		process(case, control, filename)
		break