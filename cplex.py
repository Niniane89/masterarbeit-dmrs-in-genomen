def solve(C_i, N_i,  nsamples = 3):
	"""loest das Optimierungsproblem mit Cplex
	C_i: Matrix mit methylierten Reads
	N_i: Matrix mit Gesamtanzahl alignierter Reads
	nsamples: Anzahl Proben in der ersten Klasse"""
	#Gewichte
	a_class = 34.83
	a_diff = 0.5* 33.50
	#Wertetabelle alpha space
	a_space_table = precompute_a_space(C_i.index, 0.0018, 0.054, 3.0) 

	n = N_i.shape[0] #Anzahl CpGs
	samples = N_i.shape[1] #Gesamtanzahl Proben

	c = cplex.Cplex() #initialisieren
	c.objective.set_sense(c.objective.sense.minimize) #Art der Optimierung
	#Variablennamen und Grenzen festlegen
	names = [] #Variablennamen
	names2 = [] #Hilfsvariablennamen
	for i in range(n):
		names.extend(["mu_"+str(i)+"_plus"]) #Klassen
		names.extend(["mu_"+str(i)+"_minus"])
		#Hilfsvariablen fuer Betrag in Term 3 der ZF (space)
		names2.extend(["z_plus_"+str(i)]) 
		names2.extend(["z_minus_"+str(i)])
		names2.extend(["z_diff_"+str(i)]) #Hilfsvariablen fuer Betrag in Term 4 der ZF (diff)
		for t in range(samples):
			names.extend(["mu_"+str(i)+"_"+str(t)]) #mu_i_t
			names2.extend(["z_"+str(i)+"_"+str(t)]) #z_i_t Hilfsvariablen fuer Betrag in (2) (class)
	#Grenzen fuer "normale" Variablen
	lb = [0 for _ in names]
	ub = [1 for _ in names]
	#zu den Variablen des Problems hinzufuegen, zunaechst nur variablenname und obere/untere Grenze
	c.variables.add(names = names, lb = lb, ub = ub)
	lb = [0 for _ in names2] # z >= 0
	c.variables.add(names = names2, lb = lb)
	###############################################################
	constraints = []
	for i in range(n):
		for t in range(samples):
			N = N_i.iloc[i, t]
			C = C_i.iloc[i, t]
			#Zielfunktion -> linearer Anteil
			c.objective.set_linear("mu_"+str(i)+"_"+str(t), (-C*(N+2)**2)/ ((C+1)*(N-C+1)) )#(a-b)^2 -> -2 ab
			c.objective.set_linear("z_"+str(i)+"_"+str(t), a_class ) #Hilfsvariable fuer Betrage in Term 2
			#Zielfunktion -> quadratischer Anteil
			c.objective.set_quadratic_coefficients("mu_"+str(i)+"_"+str(t), "mu_"+str(i)+"_"+str(t), (N*(N+2)**2)/((C+1)*(N-C+1)) ) #2, da Faktor 1/2, Gewichtung
			constraints.append(cplex.SparsePair(ind = ["z_"+str(i)+"_"+str(t), "mu_"+str(i)+"_"+str(t), "mu_"+str(i)+"_plus" if t < nsamples else "mu_"+str(i)+"_minus" ], val = [1.0, -1.0, 1.0]))
			constraints.append(cplex.SparsePair(ind = ["z_"+str(i)+"_"+str(t), "mu_"+str(i)+"_"+str(t), "mu_"+str(i)+"_plus" if t < nsamples else "mu_"+str(i)+"_minus" ], val = [1.0, 1.0, -1.0]))
	
		if i > 1:
			c.objective.set_linear("z_plus_"+str(i), a_space_table.iloc[i-1] ) #Terme fuer space
			c.objective.set_linear("z_minus_"+str(i), a_space_table.iloc[i-1] )
			constraints.append(cplex.SparsePair(ind = ["z_plus_"+str(i), "mu_"+str(i)+"_plus", "mu_"+str(i-1)+"_plus" ], val = [1.0, -1.0, 1.0]))
			constraints.append(cplex.SparsePair(ind = ["z_plus_"+str(i), "mu_"+str(i)+"_plus", "mu_"+str(i-1)+"_plus" ], val = [1.0, 1.0, -1.0]))
			constraints.append(cplex.SparsePair(ind = ["z_minus_"+str(i), "mu_"+str(i)+"_minus", "mu_"+str(i-1)+"_minus" ], val = [1.0, -1.0, 1.0]))
			constraints.append(cplex.SparsePair(ind = ["z_minus_"+str(i), "mu_"+str(i)+"_minus", "mu_"+str(i-1)+"_minus" ], val = [1.0, 1.0, -1.0]))

		constraints.append(cplex.SparsePair(ind = ["z_diff_"+str(i), "mu_"+str(i)+"_minus", "mu_"+str(i)+"_plus" ], val = [1.0, -1.0, 1.0]))
		constraints.append(cplex.SparsePair(ind = ["z_diff_"+str(i), "mu_"+str(i)+"_minus", "mu_"+str(i)+"_plus" ], val = [1.0, 1.0, -1.0]))
		c.objective.set_linear("z_diff_"+str(i), a_diff )
			
	###################################################################
	#lineare Constraints -> aus den Betraegen entstanden	
	c.linear_constraints.add(lin_expr = constraints,
                             senses = ["G" for _ in constraints],
                             rhs = [0.0 for _ in constraints], #rechte Seite
                             range_values = [0.0 for _ in constraints],)

	c.solve()
	
	return c



def solve_all(folder, outfolder):
	for fold in os.listdir(folder):
		print(fold)
		for filename in os.listdir(folder+fold):

			frame = pd.read_csv(folder+fold+"/"+filename, delim_whitespace = True, index_col = 0)
			frame.columns = ["M","U+M","M","U+M","M","U+M","M","U+M","M","U+M","M","U+M"]
			C_i = frame.loc[:,"M"]
			N_i = frame.loc[:,"U+M"]
			N_i.columns = np.arange(len(N_i.columns))
			C_i.columns = np.arange(len(C_i.columns))
			c = solve(C_i, N_i)
			#print(c.solution.get_status()) #statusbericht


			#output
			n = N_i.shape[0]
			samples = N_i.shape[1]
			name = outfolder+fold+"/"+filename.strip(".csv")
			with open(name + "Klassenmethylierung.csv" , "w") as f1:
				for i in range(n):
					f1.write(str(frame.index[i]) + "\t" +str(c.solution.get_values("mu_"+str(i)+"_plus")) +" \t" +str(c.solution.get_values("mu_"+str(i)+"_minus")) +"\n")
					#print("\t".join(str(c.solution.get_values("mu_"+str(i)+"_"+str(t)) ) for t in range(samples)), file=f2) 