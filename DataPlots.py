import pandas as pd
import matplotlib.pyplot as plt
from matplotlib_venn import *
import numpy as np
from pprint import pprint
from matplotlib.font_manager import FontProperties
import os
import graphics
import pickle
import Masterarbeit
import math
import itertools


def plot_synthetisch():
	A = [0, 0, 14, 163, 291, 300, 300, 300, 300, 300]
	fp = [0, 0, 0, 0, 1, 2, 2, 2, 3, 4]
	fn = [300, 300, 286, 137, 9, 0, 0, 0, 0, 0]

	create_plot("ueberlapp", "Interklassenmethylierungsdifferenz der DMCs", "Anzahl korrekt ermittelter DMCs")
	X = np.arange(0.1, 1.01, 0.1)
	p1 = plt.bar(X, A, 0.1, color="#84B818", align = "center" )
	plt.xticks(X, ([str(i) for i in X]) )
	plt.xlim(0.05,1.05)
	plt.savefig("korrekt_gefunden.pdf")
	plt.close()

	create_plot("ueberlapp", "Interklassenmethylierungsdifferenz der DMCs", "Anzahl falsch ermittelter DMCs")

	p2 = plt.bar(X, fn, 0.1,  color='b', align = "center" )
	p1 = plt.bar(X, fp, 0.1, bottom=fn,color='r', align = "center" )
	
	width = 0.1
	#plt.xticks(X+width/2., ([str(i) for i in X]) )
	plt.xticks(X, ([str(i) for i in X]) )
	#plt.yticks(np.arange(0,81,10))
	plt.xlim(0.05,1.05)
	plt.legend( (p1[0], p2[0]), ('falsch positiv', 'falsch negativ') )
	plt.savefig("falsch_gefunden.pdf")
	plt.close()



def plot_overlap2():
	"""plottet ueberlapp bei varition der gewichte"""
	A = [1722, 544, 225, 117, 56, 34, 15, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	B = [131, 43, 23, 16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	C = [2514, 732, 279, 133, 63, 35, 24, 16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	D = [89, 32, 16, 12, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	X = np.arange(0.05, 1.0, 0.05)
	create_plot("ueberlapp", "Schwellenwert", "Anzahl gefilterter DMCs")
	ax = plt.subplot(111)
	ax.plot(X, A, label="0.5 adiff")
	ax.plot(X, B, label="3 aspace")
	ax.plot(X, C, label="0.5 adiff / 3 aspace")
	ax.plot(X, D, label="normale Gewichte")
	
	set_legend_position(ax)
	plt.savefig("variation_gewichte.pdf")
	plt.close()





def plot_venn_diagramm(sets, setNames, t=0):
	"""sets: list of Sets"""
	plt.figure("Venn2", figsize=(6, 4), dpi=80)
	if len(sets) == 2:
		v = venn2(sets, setNames)
	else:
		if len(sets) ==3:
			v = venn3(sets, setNames)
		else:
			print("Anzahl Mengen ungültig")
			return


	plt.savefig("Venn_"+"+".join(x for x in setNames)+str(t)+".pdf")
	plt.close()





def plot_iteration_results(iteration, filename = ""):
	outfolder = "output/"
	if filename == "":
		store = pd.HDFStore(outfolder+str(iteration)+".h5")
	else:
		store = pd.HDFStore(outfolder+filename+".h5")
	mu_i_case = store['mu_i_case']
	mu_i_control = store['mu_i_control']
	median_case = store['median_case']
	median_control = store['median_control']
	mu_i_t_case = store['mu_i_t_case']
	mu_i_t_control = store['mu_i_t_control']


	samples = pd.HDFStore(outfolder+"0.h5")
	mu_i_t_case_sample = samples['mu_i_t_case']
	mu_i_t_control_sample = samples['mu_i_t_control']

	#zunächst werte mit startwerte
	#Plot 1 Messwerte + muicase
	plot_mu_i_t_median(filename, mu_i_t_case, mu_i_t_control)
	plot_classMethylation(filename, mu_i_case, mu_i_control)
	plot_classMethylation_with_Data(filename, mu_i_case, mu_i_control, mu_i_t_case_sample , mu_i_t_control_sample)




def plot_classMethylation(id, mu_i_case, mu_i_control):
	create_plot("Klassenmethylierungswerte", "Position", "Methylierung")
	upper = 2000
	#X = np.arange(0,upper)
	X = mu_i_case.iloc[0:upper].index


	ax = plt.subplot(111)
	ax.plot(X, mu_i_case.as_matrix()[0:upper], "r", label="Klassenmethylierungswerte case")
	ax.plot(X, mu_i_control.as_matrix()[0:upper], "b", label="Klassenmethylierungswerte control")

	box = ax.get_position()
	ax.set_position([box.x0, box.y0 + box.height * 0.1,
	box.width, box.height * 0.9])
	fontP = FontProperties()
	fontP.set_size('small')
	# Put a legend below current axis
	ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=2, prop = fontP)
	#set_legend_position(ax)

	plt.savefig(id+"_mu_i_class.svg")
	plt.close()



def plot_mu_i_t_median(id, mu_i_t_case, mu_i_t_control):
	'''plottet alle mu i t und ihren Median '''
	upper = 2000
	create_plot("Median_muit", "Position", "Methylierung")
	median = mu_i_t_case.median(axis = 1)
	median = pd.DataFrame(median)
	median_control = mu_i_t_control.median(axis = 1)

	#X = np.arange(0,upper)
	X = mu_i_t_case.iloc[0:upper, :].index

	ax = plt.subplot(111)
	ax.plot(X, median.as_matrix()[0:upper], "r", label="Median case")
	ax.plot(X, median_control.as_matrix()[0:upper], "b", label="Median control")

	#fuer alle Proben ein Punkt


	ax.plot(X, mu_i_t_case.as_matrix()[0:upper], "mx", label="Mu_i^(t) case")
	ax.plot(X, mu_i_t_control.as_matrix()[0:upper], "c.", label="Mu_i^(t) control")

	set_legend_position(ax)

	plt.savefig(id+"_mu_i_t.svg")
	plt.close()
	


def plot_classMethylation_with_Data(id, mu_i_case, mu_i_control, mu_i_t_case_sample , mu_i_t_control_sample):
	'''plottet die errechneten Klassenmethylierungswerte und die Messwerte'''
	create_plot("startwerte", "Position", "Methylierung")
	upper = 200
	#X = np.arange(0,upper)
	X = mu_i_case.iloc[0:upper].index

	ax = plt.subplot(111)
	ax.plot(X, mu_i_case.as_matrix()[0:upper], "r", label="Klassenmethylierungswerte case")
	ax.plot(X, mu_i_control.as_matrix()[0:upper], "b", label="Klassenmethylierungswerte control")
	pprint(mu_i_t_case_sample.as_matrix()[0:upper])
	ax.plot(X, mu_i_t_case_sample.as_matrix()[0:upper], "kx", label="Mu_i^(t) case")
	ax.plot(X, mu_i_t_control_sample.as_matrix()[0:upper], "k.", label="Mu_i^(t) control")

	#Aussehen konfigurieren
	box = ax.get_position()
	ax.set_position([box.x0, box.y0 + box.height * 0.1,
	box.width, box.height * 0.9])
	fontP = FontProperties()
	fontP.set_size('small')
	# Put a legend below current axis
	ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=4, prop = fontP)
	#set_legend_position(ax)

	plt.savefig(id+"_mu_i_class_Data.svg")
	plt.close()


def plot_distances_aclass(folder):
	"""plottet die Abstände von muit zum Mittelwert/ Median"""
	list_of_distances_mean = pickle.load( open( folder+"list_of_distances_mean_all.p", "rb" ) )
	list_of_distances_median = pickle.load( open( folder+"list_of_distances_median_all.p", "rb" ) )
	list_of_distances_median= sorted(list_of_distances_median)
	#n = len(list_of_distances_median)
	#b = int(n/3)
	#list_of_distances_median = list_of_distances_median[b:n]
	#X = np.arange(len(list_of_distances_median))
	create_plot("Abstaende muit", "Anzahl", "Abstand")
	ax = plt.subplot(111)
	ax.plot(list_of_distances_median, "b", label="Abstand zum Median")
	ax.plot(list_of_distances_mean, "r", label="Abstand zum Mittelwert")
	set_legend_position_below(ax)
	plt.savefig(folder+"list_of_distances_aclass.pdf")
	plt.close()



def plot_distances_adiff(folder):
	"""plottet die Abstände von muit zum Mittelwert/ Median"""
	list_of_distances_mean = pickle.load( open( folder+"list_of_class_distances_mean_all.p", "rb" ) )
	list_of_distances_median = pickle.load( open( folder+"list_of_class_distances_median_all.p", "rb" ) )

	create_plot("Interklassendistanz", "Anzahl", "Interklassendistanz")
	ax = plt.subplot(111)
	ax.plot(list_of_distances_median, "b", label="Abstand zum Median")
	ax.plot(list_of_distances_mean, "r", label="Abstand zum Mittelwert")
	set_legend_position_below(ax)
	plt.savefig(folder+"list_of_distances_adiff.pdf")
	plt.close()






def create_plot(figurename, xlabel, ylabel):
	plt.figure(figurename, figsize=(8, 6), dpi=80)
	plt.xlabel(xlabel) #Bezeichung x-Achse
	plt.ylabel(ylabel) #Bezeichnung y-Achse


def set_legend_position(ax):
	"""Legende rechts neben Grafik"""
	# Shrink current axis by 20%
	box = ax.get_position()
	ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

	# Put a legend to the right of the current axis
	fontP = FontProperties()
	fontP.set_size('small')
	ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), fancybox=True, shadow=True, prop = fontP)



def set_legend_position_below(ax, ncols = 2):
	"""setzt die Legende von ax unter die grafik"""
	#Aussehen konfigurieren
	box = ax.get_position()
	ax.set_position([box.x0, box.y0 + box.height * 0.1,
	box.width, box.height * 0.9])
	fontP = FontProperties()
	fontP.set_size('small')
	# Put a legend below current axis
	ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15), fancybox=True, shadow=True, ncol=ncols, prop = fontP)


def plotCplex_vergleich():
	folder1 = "cplex_gewichte/"
	folder2 = "cplex/"
	outfolder = "output/"
	#filenames = ["200_chr1_402383", "200_chr1_5386995", "200_chr1_3068380"]
	filenames = ["200_chr8_26157893", "200_chr3_195086523", "200_chr3_38802644"]
	for filename in filenames:
		frame = pd.read_csv(folder1 + filename +"Klassenmethylierung.csv", delim_whitespace = True, header = None)
		frame2 = pd.read_csv(folder2 + filename +"Klassenmethylierung.csv", delim_whitespace = True, header = None)
		store = pd.HDFStore(outfolder+filename+".h5")
		mu_i_case = store['mu_i_case']
		mu_i_control = store['mu_i_control']

		create_plot("Klassenmethylierungswerte", "Position", "Methylierung")
		upper = frame.shape[0]

		X = np.arange(0,upper)

		#X =box mu_i_case.iloc[0:upper].index


		ax = plt.subplot(111)
		ax.plot(X, frame.iloc[:,0].as_matrix(), "r", label="Klassenmethylierungswerte case")
		ax.plot(X, frame.iloc[:,1].as_matrix(), "b", label="Klassenmethylierungswerte control")

		ax.plot(X, frame2.iloc[:,0].as_matrix(), "m", label="case ohne Gewichte")
		ax.plot(X, frame2.iloc[:,1].as_matrix(), "c", label="control ohne Gewichte")

		#ax.plot(X, mu_i_case.as_matrix(), "g", label="case eigenes Verfahren")
		#ax.plot(X, mu_i_case.as_matrix(), "y", label="control eigenes Verfahren")

		box = ax.get_position()
		ax.set_position([box.x0, box.y0 + box.height * 0.1,
		box.width, box.height * 0.9])
		fontP = FontProperties()
		fontP.set_size('small')
		# Put a legend below current axis
		ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=2, prop = fontP)
		#set_legend_position(ax)



		plt.savefig(filename+"_mu_i_class.svg")
		plt.close()
		





def compplot(folder, result_folder,filename, chromosom = "chr1", l = 7733834, r = 7735052 ):
	"""folder: Ordner fuer die Rohdaten
	result_folder: Ordner, in dem sich die Ergebnisse befinden
	filename: filename+Klassenmethylierung.csv sind der Name der Datei, in der die Ergebnissestehen.
	chromosom: das betrachtete Chromosom
	l: linke cpg-Position
	r: rechte cpg position"""

	
	reg = str(l)+"_"+str(r)#string fuer plot
	
	df = pd.read_csv(folder+chromosom+"/"+filename+".csv", delim_whitespace = True, index_col = 0, header = 0)
	df = df.loc[l:r]

	df.columns = ["M","U+M","M","U+M","M","U+M","M","U+M","M","U+M","M","U+M"]
	N = df.loc[:,"U+M"].transpose()
	C = df.loc[:,"M"].transpose()

	graphics.plot_comparative(C, N, chromosom+"_" + reg)
	result_df =  pd.read_csv(result_folder+chromosom+"/"+filename+"Klassenmethylierung.csv", delim_whitespace = True, index_col = 0, header = None)

	result_df = result_df.loc[l:r].transpose()
	print(result_df)
	graphics.plot_comparative(C, N, chromosom+"_"+reg, show_results = True, results = result_df)








def Coverage_histogram(df):
	"""erstellt ein Histogramm ueber die Coverage im uebergebenen DataFrame"""
	array = np.array(df.as_matrix())
	create_plot("Histogramm", "Abdeckung (Coverage)", "Anzahl CpGs")
	
	hist, bin_edges = np.histogram(array, bins=np.arange(array.max()+2))
	print(np.mean(array))
	#plt.plot(bins, counts)

	plt.bar(bin_edges[:-1], hist, width = 1, log = True, color = "#84B818", edgecolor = "#84B818")
	plt.xlim(min(bin_edges), max(bin_edges))  
	plt.savefig("Histogramm.pdf")



def CpG_histogram(folder):
	"""erzeugt fuer alle chromosomen ein Histogramm über die Anzahl der CpGs"""
	
	X = np.arange(21)
	Y = []
	for i in range(21):
		chromosom = "chr"+str(i+1)+".bed"
		frame = pd.read_csv(folder+chromosom, delim_whitespace = True, index_col = 0)
		Y.append(len(frame.index))
	print(sum(Y))
	create_plot("Histogramm", "Chromosom", "Anzahl CpGs")
	print(Y)
	print(X)
	width = 0.8
	ax = plt.subplot(111)

	ax.bar(X, Y, width,  color = "#84B818")

	names = [str(i) for i in itertools.chain(range(1,21) , ["X"])]
	ax.set_xticks(np.arange(width/2.,21+width/2.))
	ax.set_xticklabels( names)
	
	#plt.xticks([width/2.,21+width/2., 5], names )
	plt.xlim(0, 21)  
	plt.savefig("Histogramm_AnzahlCpg.pdf")







def plot_hist(data, bin_size, filename, xAchse = "Interklassenmethylierungsdifferenz", yAchse = "Anzahl", r = (0,1)):
	"""plottet ein Histogramm der in data uebergebenen Daten
	range: (x, y)"""
	create_plot("Histogramm", xAchse, yAchse)
	bin_anzahl = int((r[1]-r[0] ) /bin_size)
	print(bin_anzahl)

	plt.hist(data, range = r,  bins = bin_anzahl, color = "#84B818")
	plt.xlim(r[0], r[1]) 
	plt.savefig(filename)
	plt.close()



	



if __name__ == "__main__":
	df = pd.read_csv("G:/Masterarbeit/cplex/adiff_halbiert/QPresults.bed", delim_whitespace = True, index_col = [0,1], header = 0)
	array = np.array(df.iloc[:,1].as_matrix())
	plot_hist(array, 0.01, "Histogramm_results.pdf")
